#include <graph-reader/reader/seq-reader.hxx>
#include <graph-reader/reader/vcc-reader.hxx>
#include <graph-reader/reader/vcsg-reader.hxx>
#include <graph-reader/reader/ecc-reader.hxx>
#include <graph-reader/reader/ecsg-reader.hxx>
#include <graph-reader/cxxopts/cxxopts.hpp>
#include <libgraph-processing/utility.hxx>
#include <iostream>
#include <fstream>

using namespace stg;

cxxopts::ParseResult parse(int argc, char* argv[]);

graph_props extract_props(const std::string& in);

int main (int argc, char* argv[])
{
    using namespace std;

    auto result = parse(argc, argv);

    auto positional = result["positional"].as<vector<string>>();
    if(result.count("positional") < 1)
    {
        cerr << "Missing positional arguments [input output]" << endl;
        return 1;
    }

    string query = result["query"].as<string>();
    if(!(query == "check-edge" ||
         query == "n-vertices" ||
         query == "n-edges" ||
         query == "out-degree" ||
         query == "in-degree" ||
         query == "out-neighbours" ||
         query == "in-neighbours" ||
         query == "print" ||
         query == "bfs" ||
         query == "dfs" ||
         query == "cc" ||
         query == "spsp" ||
         query == "sssp" ||
         query == "apsp" ||
         query == "out-degree-c" ||
         query == "in-degree-c" ||
         query == "betweenness-c" ||
         query == "closeness-c" ||
         query == "pagerank"))
    {
        cerr << query
            << " is not a valid option for --query parameter and should be"
            << " check-edge, n-vertices, n-edges, out-degree, in-degree,"
            << " out-neighbours, in-neighbours, print, bfs, dfs, cc,"
            << " spsp, sssp, apsp, out-degree-c, in-degree-c, closeness-c,"
            << " betweenness-c, or pagerank" << endl;
        return 1;
    }

    string approach = result["approach"].as<string>();
    if(!(approach == "sequential" ||
         approach == "vertex-centric" ||
         approach == "edge-centric"))
    {
        cerr << approach
            << " is not a valid option for --approach parameter and should be"
            << " sequential, vertex-centric, or edge-centric" << endl;
        return 1;
    }

    string style = result["style"].as<string>();
    if(!(style == "compute" ||
         style == "scatter-gather"))
    {
        cerr << style
            << " is not a valid option for --style parameter and should be"
            << " compute, or scatter-gather" << endl;
        return 1;
    }


    execution exec;

    if(approach == "sequential")
        exec = execution::seq;

    if(approach == "vertex-centric")
    {
        if(style == "compute")
            exec = execution::vcc;
        else
            exec = execution::vcsg;
    }

    if(approach == "edge-centric")
    {
        if(style == "compute")
            exec = execution::ecc;
        else
            exec = execution::ecsg;
    }


    auto props = extract_props(positional[0]);
    uint32_t u = result["vertex-u"].as<uint32_t>();
    uint32_t v = result["vertex-v"].as<uint32_t>();

    if(props.format == "edge-list")
    {
        if(approach == "sequential")
        {
            if(props.weighted)
                run_weighted_edge_list_seq(positional[0], query, u, v);
            else
                run_edge_list_seq(positional[0], query, u, v);
        }
        else if(approach == "vertex-centric")
        {
            if(style == "compute")
            {
                if(props.weighted)
                    run_weighted_edge_list_vcc(positional[0], query, u, v);
                else
                    run_edge_list_vcc(positional[0], query, u, v);
            }
            else
            {
                if(props.weighted)
                    run_weighted_edge_list_vcsg(positional[0], query, u, v);
                else
                    run_edge_list_vcsg(positional[0], query, u, v);
            }
        }
        else
        {
            if(style == "compute")
            {
                if(props.weighted)
                    run_weighted_edge_list_ecc(positional[0], query, u, v);
                else
                    run_edge_list_ecc(positional[0], query, u, v);
            }
            else
            {
                if(props.weighted)
                    run_weighted_edge_list_ecsg(positional[0], query, u, v);
                else
                    run_edge_list_ecsg(positional[0], query, u, v);
            }
        }
    }
    else if(props.format == "adj-list")
    {
        if(approach == "sequential")
        {
            if(props.weighted)
                run_weighted_adj_list_seq(positional[0], query, u, v, props);
            else
                run_adj_list_seq(positional[0], query, u, v, props);
        }
        else if(approach == "vertex-centric")
        {
            if(style == "compute")
            {
                if(props.weighted)
                    run_weighted_adj_list_vcc(positional[0], query, u, v, props);
                else
                    run_adj_list_vcc(positional[0], query, u, v, props);
            }
            else
            {
                if(props.weighted)
                    run_weighted_adj_list_vcsg(positional[0], query, u, v, props);
                else
                    run_adj_list_vcsg(positional[0], query, u, v, props);
            }
        }
        else
        {
            if(style == "compute")
            {
                if(props.weighted)
                    run_weighted_adj_list_ecc(positional[0], query, u, v, props);
                else
                    run_adj_list_ecc(positional[0], query, u, v, props);
            }
            else
            {
                if(props.weighted)
                    run_weighted_adj_list_ecsg(positional[0], query, u, v, props);
                else
                    run_adj_list_ecsg(positional[0], query, u, v, props);
            }
        }
    }
    else if(props.format == "edge-grid")
    {
        if(approach == "sequential")
        {
            if(props.weighted)
                run_weighted_edge_grid_seq(positional[0], query, u, v, props);
            else
                run_edge_grid_seq(positional[0], query, u, v, props);
        }
        else if(approach == "vertex-centric")
        {
            if(style == "compute")
            {
                if(props.weighted)
                    run_weighted_edge_grid_vcc(positional[0], query, u, v, props);
                else
                    run_edge_grid_vcc(positional[0], query, u, v, props);
            }
            else
            {
                if(props.weighted)
                    run_weighted_edge_grid_vcsg(positional[0], query, u, v, props);
                else
                    run_edge_grid_vcsg(positional[0], query, u, v, props);
            }
        }
        else
        {
            if(style == "compute")
            {
                if(props.weighted)
                    run_weighted_edge_grid_ecc(positional[0], query, u, v, props);
                else
                    run_edge_grid_ecc(positional[0], query, u, v, props);
            }
            else
            {
                if(props.weighted)
                    run_weighted_edge_grid_ecsg(positional[0], query, u, v, props);
                else
                    run_edge_grid_ecsg(positional[0], query, u, v, props);
            }
        }
    }
    else
    {
        cerr << "Unknown format" << endl;
    }
}

cxxopts::ParseResult parse(int argc, char* argv[])
{
    using namespace std;

    try {
        cxxopts::Options options("graph-reader",
         "This program reads the files indexed by graph-indexer");

        options.positional_help("input.idx").show_positional_help();

        options.add_options()
            ("q,query", "Query to execute, can be check-edge, n-vertices,"
             " n-edges, out-degree, in-degree, out-neighbours, in-neigbours"
             " print, bfs, dfs, cc, spsp, sssp, apsp, out-degree-c,"
             " in-degree-c, closeness-c, betweenness-c, or pagerank",
             cxxopts::value<string>()->default_value("print"))
            ("a,approach", "Approach to execute query, can be sequential, vertex-centric, or edge-centric",
             cxxopts::value<string>()->default_value("sequential"))
            ("s,style", "Style of API, can be compute, or scatter-gather",
             cxxopts::value<string>()->default_value("compute"))
            ("u,vertex-u", "Set source vertex for check-edge, and neighbours queries",
             cxxopts::value<uint32_t>()->default_value("0"))
            ("v,vertex-v", "Set target vertex for check-edge query",
             cxxopts::value<uint32_t>()->default_value("0"))
            ("positional", "Positional arguments are: an .idx input",
             cxxopts::value<std::vector<std::string>>())
            ("h,help", "Print help");

        options.parse_positional("positional");
        auto result = options.parse(argc, argv);

        if(result.count("help"))
        {
            cout << options.help() << endl;
            exit(0);
        }

        return result;

    } catch(const cxxopts::OptionException& e) {
        cerr << "error parsing options: " << e.what() << std::endl;
        exit(1);
    }
}

graph_props extract_props(const std::string& in)
{
    auto type = extract_graph_type(in);

    if(!type)
        std::abort();

    return graph_type_to_graph_props(*type);
}
