#include <graph-reader/reader/ecc-reader.hxx>

/* #include <graph-reader/algo/ec-bfs.hxx> */
/* #include <graph-reader/algo/ec-dfs.hxx> */
/* #include <graph-reader/algo/ec-cc.hxx> */
/* #include <graph-reader/algo/ec-spsp.hxx> */
/* #include <graph-reader/algo/ec-sssp.hxx> */
/* #include <graph-reader/algo/ec-apsp.hxx> */
/* #include <graph-reader/algo/ec-outdegree-centrality.hxx> */
/* #include <graph-reader/algo/ec-indegree-centrality.hxx> */
/* #include <graph-reader/algo/ec-closeness-centrality.hxx> */
/* #include <graph-reader/algo/ec-betweenness-centrality.hxx> */
/* #include <graph-reader/algo/ec-pagerank.hxx> */
#include <libgraph-processing/disk-edge-list.hxx>
#include <libgraph-processing/disk-adj-list.hxx>
#include <libgraph-processing/disk-edge-grid.hxx>
#include <libgraph-processing/edge.hxx>

#include <iostream>

template <typename GraphEngine>
void run_ecc(const std::string& index, const std::string& query,
            uint32_t u, uint32_t v)
{
    using namespace std;

    GraphEngine graph(index);

    cerr << "Not implemented yet" << endl;
}

void run_edge_list_ecc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_ecc<disk_edge_list<edge>>(index, query, u, v);
}

void run_adj_list_ecc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_ecc<disk_adj_list<edge>>(index, query, u, v);
}

void run_edge_grid_ecc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_ecc<disk_edge_grid<edge>>(index, query, u, v);
}


void run_weighted_edge_list_ecc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_ecc<disk_edge_list<weighted_edge>>(index, query, u, v);
}

void run_weighted_adj_list_ecc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_ecc<disk_adj_list<weighted_edge>>(index, query, u, v);
}

void run_weighted_edge_grid_ecc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_ecc<disk_edge_grid<weighted_edge>>(index, query, u, v);
}
