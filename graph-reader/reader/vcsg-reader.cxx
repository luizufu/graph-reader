#include <graph-reader/reader/vcsg-reader.hxx>

#include <graph-reader/algo/vcsg-bfs.hxx>
#include <graph-reader/algo/vcsg-dfs.hxx>
#include <graph-reader/algo/vcsg-cc.hxx>
#include <graph-reader/algo/vcsg-spsp.hxx>
#include <graph-reader/algo/vcsg-sssp.hxx>
#include <graph-reader/algo/vcsg-apsp.hxx>
#include <graph-reader/algo/vcsg-outdegree-centrality.hxx>
#include <graph-reader/algo/vcsg-indegree-centrality.hxx>
#include <graph-reader/algo/vcsg-closeness-centrality.hxx>
#include <graph-reader/algo/vcsg-betweenness-centrality.hxx>
#include <graph-reader/algo/vcsg-pagerank.hxx>
#include <libgraph-processing/disk-edge-list.hxx>
#include <libgraph-processing/disk-adj-list.hxx>
#include <libgraph-processing/disk-edge-grid.hxx>
#include <libgraph-processing/edge.hxx>

#include <iostream>

template <typename GraphEngine>
void run_vcsg(const std::string& index, const std::string& query,
            uint32_t u, uint32_t v)
{
    using namespace std;

    GraphEngine graph(index);

    if(query == "bfs")
    {
        vcsg_bfs search(graph, u);
        for(auto w : search.traverse())
            cout << w << '\n';
    }
    else if(query == "dfs")
    {
        vcsg_dfs search(graph, u);
        for(auto w : search.traverse())
            cout << w << '\n';
    }
    else if(query == "cc")
    {
        vcsg_cc search(graph);
        for(auto [w, c] : search.traverse())
            cout << w << " (" << (int) c << ")\n";
    }
    else if(query == "spsp")
    {
        vcsg_spsp search(graph, u, v);
        for(auto [v, w] : search.traverse())
            cout << v << " (" << w << ") <- ";
    }
    else if(query == "sssp")
    {
        vcsg_sssp search(graph, u);

        for(auto path : search.traverse())
        {
            for(auto [v, w] : path)
                cout << v << " (" << w << ") <- ";

            cout << '\n';
        }
    }
    else if(query == "apsp")
    {
        vcsg_apsp search(graph);

        for(auto path : search.traverse())
        {
            for(auto [v, w] : path)
                cout << v << " (" << w << ") <- ";

            cout << '\n';
        }
    }
    else if(query == "out-degree-c")
    {
        vcsg_outdegree_centrality centralities(graph);
        for(auto [w, r] : centralities.traverse())
            cout << w << " (" << r << ")\n";
    }
    else if(query == "in-degree-c")
    {
        vcsg_indegree_centrality centralities(graph);
        for(auto [w, r] : centralities.traverse())
            cout << w << " (" << r << ")\n";
    }
    else if(query == "closeness-c")
    {
        vcsg_closeness_centrality centralities(graph);
        for(auto [w, r] : centralities.traverse())
            cout << w << " (" << r << ")\n";
    }
    else if(query == "betweenness-c")
    {
        vcsg_betweenness_centrality centralities(graph);
        for(auto [w, r] : centralities.traverse())
            cout << w << " (" << r << ")\n";
    }
    else if(query == "pagerank")
    {
        vcsg_pagerank ranks(graph);
        for(auto [w, r] : ranks.traverse())
            cout << w << " (" << r << ")\n";
    }
    else
    {
        cerr << "Not a valid query for approach=vertex-centric" << endl;
    }
}

void run_edge_list_vcsg(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_vcsg<disk_edge_list<edge>>(index, query, u, v);
}

void run_adj_list_vcsg(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_vcsg<disk_adj_list<edge>>(index, query, u, v);
}

void run_edge_grid_vcsg(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_vcsg<disk_edge_grid<edge>>(index, query, u, v);
}

void run_weighted_edge_list_vcsg(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_vcsg<disk_edge_list<weighted_edge>>(index, query, u, v);
}

void run_weighted_adj_list_vcsg(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_vcsg<disk_adj_list<weighted_edge>>(index, query, u, v);
}

void run_weighted_edge_grid_vcsg(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_vcsg<disk_edge_grid<weighted_edge>>(index, query, u, v);
}
