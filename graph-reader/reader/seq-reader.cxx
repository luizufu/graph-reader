#include <graph-reader/reader/seq-reader.hxx>

#include <graph-reader/algo/seq-bfs.hxx>
#include <graph-reader/algo/seq-dfs.hxx>
#include <graph-reader/algo/seq-cc.hxx>
#include <graph-reader/algo/seq-spsp.hxx>
#include <graph-reader/algo/seq-sssp.hxx>
#include <graph-reader/algo/seq-apsp.hxx>
#include <graph-reader/algo/seq-outdegree-centrality.hxx>
#include <graph-reader/algo/seq-indegree-centrality.hxx>
#include <graph-reader/algo/seq-closeness-centrality.hxx>
#include <graph-reader/algo/seq-betweenness-centrality.hxx>
#include <graph-reader/algo/seq-pagerank.hxx>
#include <libgraph-processing/disk-edge-list.hxx>
#include <libgraph-processing/disk-adj-list.hxx>
#include <libgraph-processing/disk-edge-grid.hxx>
#include <libgraph-processing/edge.hxx>

#include <iostream>

template <typename GraphEngine>
void run_seq(const std::string& index, const std::string& query,
            uint32_t u, uint32_t v)
{
    using namespace std;

    GraphEngine graph(index);

    if(query == "check-edge")
    {
        cout << graph.has_edge(u, v) << endl;
    }
    else if(query == "n-vertices")
    {
        cout << graph.n_vertices() << endl;
    }
    else if(query == "n-edges")
    {
        cout << graph.n_edges() << endl;
    }
    else if(query == "out-degree")
    {
        cout << graph.out_degree(u) << endl;
    }
    else if(query == "in-degree")
    {
        cout << graph.in_degree(u) << endl;
    }
    else if(query == "out-neighbours")
    {
        for(auto n : graph.out_neighbours(u))
            cout << n.v << '\n';
    }
    else if(query == "in-neighbours")
    {
        for(auto n : graph.in_neighbours(v))
            cout << n.v << '\n';
    }
    else if(query == "print")
    {
        for(auto e : graph.edges())
            cout << e.u << ' ' << e.v << '\n';
    }
    else if(query == "bfs")
    {
        seq_bfs search(graph, u);
        for(auto w : search.traverse())
            cout << w << '\n';
    }
    else if(query == "dfs")
    {
        seq_dfs search(graph, u);
        for(auto w : search.traverse())
            cout << w << '\n';
    }
    else if(query == "cc")
    {
        seq_cc search(graph);
        for(auto [w, c] : search.traverse())
            cout << w << " (" << (int) c << ")\n";
    }
    else if(query == "spsp")
    {
        seq_spsp search(graph, u, v);
        for(auto [v, w] : search.traverse())
            cout << v << " (" << w << ") <- ";
    }
    else if(query == "sssp")
    {
        seq_sssp search(graph, u);

        for(auto path : search.traverse())
        {
            for(auto [v, w] : path)
                cout << v << " (" << w << ") <- ";

            cout << '\n';
        }
    }
    else if(query == "apsp")
    {
        seq_apsp search(graph);

        for(auto path : search.traverse())
        {
            for(auto [v, w] : path)
                cout << v << " (" << w << ") <- ";

            cout << '\n';
        }
    }
    else if(query == "out-degree-c")
    {
        seq_outdegree_centrality centralities(graph);
        for(auto [w, r] : centralities.traverse())
            cout << w << " (" << r << ")\n";
    }
    else if(query == "in-degree-c")
    {
        seq_indegree_centrality centralities(graph);
        for(auto [w, r] : centralities.traverse())
            cout << w << " (" << r << ")\n";
    }
    else if(query == "closeness-c")
    {
        seq_closeness_centrality centralities(graph);
        for(auto [w, r] : centralities.traverse())
            cout << w << " (" << r << ")\n";
    }
    else if(query == "betweenness-c")
    {
        seq_betweenness_centrality centralities(graph);
        for(auto [w, r] : centralities.traverse())
            cout << w << " (" << r << ")\n";
    }
    else if(query == "pagerank")
    {
        seq_pagerank ranks(graph);
        for(auto [w, r] : ranks.traverse())
            cout << w << " (" << r << ")\n";
    }
    else
    {
        cerr << "Not a valid query for approach=sequential" << endl;
    }
}

void run_edge_list_seq(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_seq<disk_edge_list<edge>>(index, query, u, v);
}

void run_adj_list_seq(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_seq<disk_adj_list<edge>>(index, query, u, v);
}

void run_edge_grid_seq(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_seq<disk_edge_grid<edge>>(index, query, u, v);
}

void run_weighted_edge_list_seq(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_seq<disk_edge_list<weighted_edge>>(index, query, u, v);
}

void run_weighted_adj_list_seq(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_seq<disk_adj_list<weighted_edge>>(index, query, u, v);
}

void run_weighted_edge_grid_seq(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_seq<disk_edge_grid<weighted_edge>>(index, query, u, v);
}
