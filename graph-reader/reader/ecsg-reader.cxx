#include <graph-reader/reader/ecsg-reader.hxx>

#include <graph-reader/algo/ecsg-bfs.hxx>
#include <graph-reader/algo/ecsg-dfs.hxx>
#include <graph-reader/algo/ecsg-cc.hxx>
#include <graph-reader/algo/ecsg-spsp.hxx>
#include <graph-reader/algo/ecsg-sssp.hxx>
#include <graph-reader/algo/ecsg-apsp.hxx>
#include <graph-reader/algo/ecsg-outdegree-centrality.hxx>
#include <graph-reader/algo/ecsg-indegree-centrality.hxx>
#include <graph-reader/algo/ecsg-closeness-centrality.hxx>
#include <graph-reader/algo/ecsg-betweenness-centrality.hxx>
#include <graph-reader/algo/ecsg-pagerank.hxx>

#include <libgraph-processing/disk-edge-list.hxx>
#include <libgraph-processing/disk-adj-list.hxx>
#include <libgraph-processing/disk-edge-grid.hxx>
#include <libgraph-processing/disk-dynamic-adj-list.hxx>
#include <libgraph-processing/disk-dynamic-edge-grid.hxx>
#include <libgraph-processing/helpers/disk-block-vector.hxx>
#include <libgraph-processing/edge.hxx>


#include <iostream>

template <typename GraphEngine>
void run_ecsg(const GraphEngine graph, const std::string& query,
            uint32_t u, uint32_t v)
{
    using namespace std;

    if(query == "bfs")
    {
        ecsg_bfs search(graph, u);
        for(auto w : search.traverse())
            cout << w << '\n';
    }
    else if(query == "dfs")
    {
        ecsg_dfs search(graph, u);
        for(auto w : search.traverse())
            cout << w << '\n';
    }
    else if(query == "cc")
    {
        ecsg_cc search(graph);
        for(auto [w, c] : search.traverse())
            cout << w << " (" << (int) c << ")\n";
    }
    else if(query == "spsp")
    {
        ecsg_spsp search(graph, u, v);
        for(auto [v, w] : search.traverse())
            cout << v << " (" << w << ") <- ";
    }
    else if(query == "sssp")
    {
        ecsg_sssp search(graph, u);

        for(auto path : search.traverse())
        {
            for(auto [v, w] : path)
                cout << v << " (" << w << ") <- ";

            cout << '\n';
        }
    }
    else if(query == "apsp")
    {
        ecsg_apsp search(graph);

        for(auto path : search.traverse())
        {
            for(auto [v, w] : path)
                cout << v << " (" << w << ") <- ";

            cout << '\n';
        }
    }
    else if(query == "out-degree-c")
    {
        ecsg_outdegree_centrality centralities(graph);
        for(auto [w, r] : centralities.traverse())
            cout << w << " (" << r << ")\n";
    }
    else if(query == "in-degree-c")
    {
        ecsg_indegree_centrality centralities(graph);
        for(auto [w, r] : centralities.traverse())
            cout << w << " (" << r << ")\n";
    }
    else if(query == "closeness-c")
    {
        ecsg_closeness_centrality centralities(graph);
        for(auto [w, r] : centralities.traverse())
            cout << w << " (" << r << ")\n";
    }
    else if(query == "betweenness-c")
    {
        ecsg_betweenness_centrality centralities(graph);
        for(auto [w, r] : centralities.traverse())
            cout << w << " (" << r << ")\n";
    }
    else if(query == "pagerank")
    {
        ecsg_pagerank ranks(graph);
        for(auto [w, r] : ranks.traverse())
            cout << w << " (" << r << ")\n";
    }
    else
    {
        cerr << "Not a valid query for approach=vertex-centric" << endl;
    }
}

void run_edge_list_ecsg(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v, const graph_props& props, const graph_header&)
{
    disk_edge_list<edge> graph(index);
    run_ecsg(graph, query, u, v);
}

void run_adj_list_ecsg(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v, const graph_props& props, const graph_header& header)
{
    graph_type type = graph_props_to_graph_type(props);

    if(props.dynamic)
    {
        if(props.weighted)
        {
            disk_dynamic_adj_list<weighted_edge> graph(index, type, header.direction);
            run_ecsg(graph, query, u, v);
        }
        else
        {
            disk_dynamic_adj_list<edge> graph(index, type, header.direction);
            run_ecsg(graph, query, u, v);
        }
    }
    else
    {
        if(props.weighted)
        {
            disk_adj_list<weighted_edge> graph(index);
            run_ecsg(graph, query, u, v);
        }
        else
        {
            disk_adj_list<edge> graph(index);
            run_ecsg(graph, query, u, v);
        }
    }
}

void run_edge_grid_ecsg(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v, const graph_props& props, const graph_header& header)
{
    graph_type type = graph_props_to_graph_type(props);

    if(props.dynamic)
    {
        if(props.weighted)
        {
            disk_dynamic_edge_grid<weighted_edge> graph(index, type, header.grid_size, header.max);
            run_ecsg(graph, query, u, v);
        }
        else
        {
            disk_dynamic_edge_grid<edge> graph(index, type, header.grid_size, header.max);
            run_ecsg(graph, query, u, v);
        }
    }
    else
    {
        if(props.weighted)
        {
            disk_edge_grid<weighted_edge> graph(index);
            run_ecsg(graph, query, u, v);
        }
        else
        {
            disk_edge_grid<edge> graph(index);
            run_ecsg(graph, query, u, v);
        }
    }
}
