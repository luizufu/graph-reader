#include <limits>
#include <numeric>

template <typename Graph>
seq_apsp<Graph>::seq_apsp(const Graph& graph)
    : _dist(graph.n_vertices(), graph.n_vertices(),
            std::numeric_limits<float>::max())
    , _prev(_dist.n_rows(), _dist.n_colums())
{
    size_t n_vertices = _prev.n_rows();

    for(auto e : graph.edges())
    {
        float w = 1.f; // change this to weighted graph
        _dist.write(e.u, e.v, e.weight());
        _prev.write(e.u, e.v, e.u);
    }

    for(uint32_t u = 0; u < n_vertices; ++u)
    {
        _dist.write(u, u, 0.f);
        _prev.write(u, u, u);
    }

    for(uint32_t k = 0; k < n_vertices; ++k)
        for(uint32_t u = 0; u < n_vertices; ++u)
            for(uint32_t v = 0; v < n_vertices; ++v)
                if(_dist.read(u, k) + _dist.read(k, v) < _dist.read(u, v))
                {
                    _dist.write(u, v, _dist.read(u, k) + _dist.read(k, v));
                    _prev.write(u, v, _prev.read(k, v));
                }
}

template <typename Graph>
cppcoro::generator<std::vector<typename seq_apsp<Graph>::path_item_t>> seq_apsp<Graph>::traverse()
{
    size_t n_vertices = _prev.n_rows();

    for(uint32_t i = 0; i < n_vertices; ++i)
    {
        for(uint32_t j = 0; j < n_vertices; ++j)
        {
            if(has_path(i, j))
            {
                std::vector<path_item_t> items;
                for(auto item : path(i, j))
                    items.push_back(item);

                co_yield items;
            }
        }
    }
}


template <typename Graph>
bool seq_apsp<Graph>::has_path(uint32_t u, uint32_t v) const
{
    return _dist.read(u, v) < std::numeric_limits<float>::max() && _prev.read(u, v) != v;
}

template <typename Graph>
float seq_apsp<Graph>::dist(uint32_t u, uint32_t v) const
{
    return _dist.read(u, v);
}

template <typename Graph>
cppcoro::generator<typename seq_apsp<Graph>::path_item_t> seq_apsp<Graph>::path(uint32_t u, uint32_t v) const
{
    co_yield { v, _dist.read(u, v) };

    while(_prev.read(u, v) != v)
    {
        v = _prev.read(u, v);
        co_yield { v , _dist.read(u, v) };
    }
}
