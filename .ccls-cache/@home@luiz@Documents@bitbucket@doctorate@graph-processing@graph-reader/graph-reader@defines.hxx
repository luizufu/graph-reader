#pragma once

#include <cstdint>

const uint32_t EDGE_LIST_ID = 0;
const uint32_t ADJ_LIST_ID = 1;
const uint32_t EDGE_GRID_ID = 2;

const uint32_t HEADER_SIZE = sizeof(uint32_t);
