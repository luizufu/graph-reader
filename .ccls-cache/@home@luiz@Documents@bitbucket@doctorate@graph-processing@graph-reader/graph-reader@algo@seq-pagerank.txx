#include <cppcoro/sync_wait.hpp>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <mutex>

template <typename Graph>
seq_pagerank<Graph>::seq_pagerank(const Graph& graph, size_t iterations, float dump)
    : _ranks(graph.n_vertices(), 1.f / graph.n_vertices())
{
    size_t n_vertices = graph.n_vertices();

    for(size_t i = 0; i < iterations; ++i)
    {
        std::vector<float> rank_new(n_vertices);

        // compute contributions (sum)
        //this way we need only out neighbours (in neighbours should be slower)
        for(uint32_t u = 0; u < n_vertices; ++u)
            for(auto n : graph.out_neighbours(u))
                rank_new[n.v] += _ranks[u] / graph.out_degree(u);

        // reuse rank_new as sum in the formula for each vertex
        for(uint32_t u = 0; u < n_vertices; ++u)
            rank_new[u] = (1.f - dump) / n_vertices + dump * rank_new[u];

        // update ranks
        _ranks = rank_new;
    }
}


template <typename Graph>
cppcoro::generator<typename seq_pagerank<Graph>::item> seq_pagerank<Graph>::traverse()
{
    for(size_t i = 0; i < _ranks.size(); ++i)
        co_yield { i, _ranks[i] };
}
