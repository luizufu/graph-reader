#pragma once

#include <libgraph-processing/disk-edge-list.hxx>
#include <libgraph-processing/disk-adj-list.hxx>
#include <libgraph-processing/disk-edge-grid.hxx>
#include <libgraph-processing/edge.hxx>


#define INSTATIATE_ALGO_TEMPLATES(algo) \
    template class algo<disk_edge_list<edge>>; \
    template class algo<disk_edge_list<weighted_edge>>; \
    template class algo<disk_adj_list<edge>>; \
    template class algo<disk_adj_list<weighted_edge>>; \
    template class algo<disk_edge_grid<edge>>; \
    template class algo<disk_edge_grid<weighted_edge>>;

