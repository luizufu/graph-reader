#pragma once

#include <vector>
#include <cppcoro/generator.hpp>

template <typename Graph>
class seq_betweenness_centrality
{
    std::vector<float> _ranks;

    using item = std::pair<uint32_t, float>;

public:

    seq_betweenness_centrality(const Graph& graph);

    cppcoro::generator<item> traverse();
};


