#include <cppcoro/sync_wait.hpp>
#include <vector>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <mutex>

template <typename Graph>
vcc_cc<Graph>::vcc_cc(const Graph& graph)
    : _engine(graph)
{
}


template <typename Graph>
cppcoro::generator<typename vcc_cc<Graph>::item> vcc_cc<Graph>::traverse()
{
    auto colors = cppcoro::sync_wait(_engine.template run_compute<uint32_t, uint32_t>(
        // compute
        [](auto&& control) {

            auto& msgs = control.messages();

            // set my color to be my id at first iteration
            if(control.superstep() == 0)
                control.set_value(control.id());

            uint32_t other_color = !msgs.empty()
                ? *std::min_element(msgs.begin(), msgs.end())
                : control.value();

            // if it's the first iteration or it has arrived a different color
            // than set my new color and send to my neighbours
            // we get only a different value less than the current one
            // I think this will no work for directed graphs?
            if(control.superstep() == 0 ||  other_color < control.value())
            {
                control.set_value(other_color);

                for(auto n : control.out_neighbours())
                    control.send_to(n.v, other_color);
            }

            // i'm done
            control.vote_to_halt();
        },
        //default_value
        0));

    std::vector<uint32_t> inds(colors.size());
    std::iota(inds.begin(), inds.end(), 0);

    std::sort(
        inds.begin(), inds.end(),
        [&colors] (uint32_t lhs, uint32_t rhs) {
            return colors[lhs] < colors[rhs];
        });

    uint32_t last_color = colors[inds[0]];
    for(auto i : inds)
    {
        co_yield { i, colors[i] };

        if(colors[i] != last_color)
            ++_count;
    }
}

/* template <typename Graph> */
/* uint8_t seq_cc<Graph>::id(uint32_t u) const */
/* { */
/*     return _ids[u]; */
/* } */

/* template <typename Graph> */
/* size_t seq_cc<Graph>::size(uint32_t u) const */
/* { */
/*     return _sizes[_ids[u]]; */
/* } */

template <typename Graph>
size_t vcc_cc<Graph>::count() const
{
    return _count;
}
