#include <graph-reader/algo/vertex_centric_engine.hxx>
#include <cppcoro/sync_wait.hpp>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <mutex>

template <typename Graph>
vcc_pagerank<Graph>::vcc_pagerank(const Graph& graph, size_t iterations, float dump)
{
    vertex_centric_engine engine(graph);

    _ranks = cppcoro::sync_wait(engine.template run_compute<float, float>(
        // compute
        [&](auto&& control) {

            auto& msgs = control.messages();

            // compute my pagerank using the formula
            // messages have the contribution of each incoming node
            // skip superstep 0
            if(control.superstep() > 0)
            {
                float sum = std::accumulate(msgs.begin(), msgs.end(), 0.f);
                control.value() = (1.f - dump)/control.n_vertices() + dump*sum;
            }

            // send my contribution to my neighbours
            if(control.superstep() < iterations)
            {
                float my_contribution = control.value() / control.out_degree();
                for(auto n : control.out_neighbours())
                    control.send_to(n.v, my_contribution);
            }
            // i'm done
            else
            {
                control.vote_to_halt();
            }
        },
        // default state (uniform distribution)
        1.f / graph.n_vertices()));
}


template <typename Graph>
cppcoro::generator<typename vcc_pagerank<Graph>::item> vcc_pagerank<Graph>::traverse()
{
    for(size_t i = 0; i < _ranks.size(); ++i)
        co_yield { i, _ranks[i] };
}
