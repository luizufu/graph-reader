#include <graph-reader/reader/vcc-reader.hxx>

#include <graph-reader/algo/vcc-bfs.hxx>
#include <graph-reader/algo/vcc-dfs.hxx>
#include <graph-reader/algo/vcc-cc.hxx>
#include <graph-reader/algo/vcc-spsp.hxx>
#include <graph-reader/algo/vcc-sssp.hxx>
#include <graph-reader/algo/vcc-apsp.hxx>
#include <graph-reader/algo/vcc-outdegree-centrality.hxx>
#include <graph-reader/algo/vcc-indegree-centrality.hxx>
#include <graph-reader/algo/vcc-closeness-centrality.hxx>
#include <graph-reader/algo/vcc-betweenness-centrality.hxx>
#include <graph-reader/algo/vcc-pagerank.hxx>
#include <libgraph-processing/disk-edge-list.hxx>
#include <libgraph-processing/disk-adj-list.hxx>
#include <libgraph-processing/disk-edge-grid.hxx>
#include <libgraph-processing/edge.hxx>

#include <iostream>

template <typename GraphEngine>
void run_vcc(const std::string& index, const std::string& query,
            uint32_t u, uint32_t v)
{
    using namespace std;

    GraphEngine graph(index);

    if(query == "bfs")
    {
        vcc_bfs search(graph, u);
        for(auto w : search.traverse())
            cout << w << '\n';
    }
    else if(query == "dfs")
    {
        vcc_dfs search(graph, u);
        for(auto w : search.traverse())
            cout << w << '\n';
    }
    else if(query == "cc")
    {
        vcc_cc search(graph);
        for(auto [w, c] : search.traverse())
            cout << w << " (" << (int) c << ")\n";
    }
    else if(query == "spsp")
    {
        vcc_spsp search(graph, u, v);
        for(auto [v, w] : search.traverse())
            cout << v << " (" << w << ") <- ";
    }
    else if(query == "sssp")
    {
        vcc_sssp search(graph, u);

        for(auto path : search.traverse())
        {
            for(auto [v, w] : path)
                cout << v << " (" << w << ") <- ";

            cout << '\n';
        }
    }
    else if(query == "apsp")
    {
        vcc_apsp search(graph);

        for(auto path : search.traverse())
        {
            for(auto [v, w] : path)
                cout << v << " (" << w << ") <- ";

            cout << '\n';
        }
    }
    else if(query == "out-degree-c")
    {
        vcc_outdegree_centrality centralities(graph);
        for(auto [w, r] : centralities.traverse())
            cout << w << " (" << r << ")\n";
    }
    else if(query == "in-degree-c")
    {
        vcc_indegree_centrality centralities(graph);
        for(auto [w, r] : centralities.traverse())
            cout << w << " (" << r << ")\n";
    }
    else if(query == "closeness-c")
    {
        vcc_closeness_centrality centralities(graph);
        for(auto [w, r] : centralities.traverse())
            cout << w << " (" << r << ")\n";
    }
    else if(query == "betweenness-c")
    {
        vcc_betweenness_centrality centralities(graph);
        for(auto [w, r] : centralities.traverse())
            cout << w << " (" << r << ")\n";
    }
    else if(query == "pagerank")
    {
        vcc_pagerank ranks(graph);
        for(auto [w, r] : ranks.traverse())
            cout << w << " (" << r << ")\n";
    }
    else
    {
        cerr << "Not a valid query for approach=vertex-centric" << endl;
    }
}

void run_edge_list_vcc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_vcc<disk_edge_list<edge>>(index, query, u, v);
}

void run_adj_list_vcc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_vcc<disk_adj_list<edge>>(index, query, u, v);
}

void run_edge_grid_vcc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_vcc<disk_edge_grid<edge>>(index, query, u, v);
}

void run_weighted_edge_list_vcc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_vcc<disk_edge_list<weighted_edge>>(index, query, u, v);
}

void run_weighted_adj_list_vcc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_vcc<disk_adj_list<weighted_edge>>(index, query, u, v);
}

void run_weighted_edge_grid_vcc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v)
{
    run_vcc<disk_edge_grid<weighted_edge>>(index, query, u, v);
}
