#include <graph-reader/algo/vertex_centric_engine.hxx>
#include <graph-reader/aux/float-comparison.hxx>
#include <cppcoro/sync_wait.hpp>
#include <algorithm>
#include <unordered_map>
#include <iostream>
#include <numeric>
#include <mutex>

template <typename Graph>
vcc_betweenness_centrality<Graph>::vcc_betweenness_centrality(const Graph& graph)
    : _ranks(graph.n_vertices(), 0.f)
{
    vertex_centric_engine engine(graph);
    size_t n_vertices = graph.n_vertices();
    float infinity = std::numeric_limits<float>::max();

    struct value
    {
        float dist = std::numeric_limits<float>::max();
        std::vector<uint32_t> pred;
        float sigma = 1.f;
    };

    struct message
    {
        uint32_t sender;
        float dist = std::numeric_limits<float>::max();
        float sigma = 0.f;
    };

    for(size_t s = 0; s < n_vertices; ++s)
    {
        auto info = cppcoro::sync_wait(engine.template run_compute<value, message>(
            // compute
            [s](auto&& control) {

                auto& msgs = control.messages();
                bool send_message = false;

                // if i'm the source than set distance to myself to 0
                // and send message to my neighbours
                if(control.superstep() == 0 && control.id() == s)
                {
                    control.value().dist = 0.f;
                    send_message = true;

                }
                // if there is message to me then i need to update
                else if(!msgs.empty())
                {
                    // find the message with minimum distance
                    auto it = std::min_element(
                        msgs.begin(), msgs.end(),
                        [](auto& lhs, auto& rhs) {
                            return lhs.dist < rhs.dist;
                        });

                    // if the message has smaller distance than what i know
                    // (there is a new shortest distance), then update my
                    // distance and reset info related to shortest paths
                    if(it->dist < control.value().dist)
                    {
                        control.value().dist = it->dist;
                        control.value().sigma = 0.f;
                        control.value().pred.clear();
                    }

                    // if message has smaller or equal distance (is shortest
                    // distance), than update info related to shortest
                    // paths and send message to my neighbours
                    if(approximately_equal(it->dist, control.value().dist))
                    {
                        control.value().sigma += it->sigma;
                        control.value().pred.push_back(it->sender);
                        send_message = true;
                    }
                }

                // send message to my neighbours actually
                if(send_message)
                {
                    message msg;
                    msg.sender = control.id();

                    for(auto n : control.out_neighbours())
                    {
                        msg.dist = control.value().dist + n.weight();
                        msg.sigma = n.weight() * control.value().sigma;

                        control.send_to(n.v, msg);
                    }
                }

                control.vote_to_halt();
            },
            // default state
            value{}));

        float max_dist = 0.f;
        for(auto& item : info)
            if(item.dist < infinity && item.dist > max_dist)
                max_dist = item.dist;

        // TODO i think the bug is that a node needs to receive all other
        // nodes contribution before computing delta values for this
        // predecessors, i don't know if this is possible in parallel...
        auto deltas = cppcoro::sync_wait(engine.template run_compute<float, float>(
            // compute
            [&info, max_dist](auto&& control) {

                // sum my delta values if there are any
                for(auto& msg : control.messages())
                    control.value() += msg;

                bool have_max_dist = approximately_equal(
                    info[control.id()].dist, max_dist);

                // if i'm the farthest target in superstep 0 or there are
                // messages to me, then calculate delta values for my
                // predecessors (in shortest paths) send message to them
                if((control.superstep() == 0 && have_max_dist)||
                   !control.messages().empty())
                {
                    size_t v = control.id();
                    for(auto u : info[v].pred)
                    {
                        float u_delta =
                            (info[u].sigma / info[v].sigma) *
                            (1 + control.value());

                        control.send_to(u, u_delta);
                    }
                }

                control.vote_to_halt();
            },
            // default state
            0.f));

        // accumulate deltas related to paths starting from s
        for(uint32_t i = 0; i < n_vertices; ++i)
            if(i != s)
                _ranks[i] += deltas[i];
    }
}

template <typename Graph>
cppcoro::generator<typename vcc_betweenness_centrality<Graph>::item> vcc_betweenness_centrality<Graph>::traverse()
{
    for(size_t i = 0; i < _ranks.size(); ++i)
        co_yield { i, _ranks[i] };
}
