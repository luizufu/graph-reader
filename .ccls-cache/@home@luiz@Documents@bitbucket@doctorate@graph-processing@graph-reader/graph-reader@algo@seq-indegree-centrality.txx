#include <cppcoro/sync_wait.hpp>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <mutex>

template <typename Graph>
seq_indegree_centrality<Graph>::seq_indegree_centrality(const Graph& graph)
    : _ranks(graph.n_vertices())
{
    for(uint32_t u = 0; u < _ranks.size(); ++u)
        _ranks[u] = graph.in_degree(u);
}


template <typename Graph>
cppcoro::generator<typename seq_indegree_centrality<Graph>::item> seq_indegree_centrality<Graph>::traverse()
{
    for(size_t i = 0; i < _ranks.size(); ++i)
        co_yield { i, _ranks[i] };
}
