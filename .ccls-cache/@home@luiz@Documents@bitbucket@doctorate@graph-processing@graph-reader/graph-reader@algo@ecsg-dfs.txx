#include <cppcoro/sync_wait.hpp>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <mutex>

template <typename Graph>
ecsg_dfs<Graph>::ecsg_dfs(const Graph& graph, uint32_t s)
    : _engine(graph)
    , _graph(graph)
    , _s(s)
{
}


template <typename Graph>
cppcoro::generator<uint32_t> ecsg_dfs<Graph>::traverse()
{
    struct value
    {
        uint32_t order = 0;
        size_t degree = 0;
        size_t i = 0;
    };

    value default_agg;
    default_agg.order = _graph.n_vertices();


    auto order = cppcoro::sync_wait(_engine.template run_scatter_gather<value, uint32_t>(
        // scatter
        [s = _s](auto&& control) {
            if(control.source_id() == s)
                control.source_value().order = 1;

            if(control.superstep() == 0)
            {
                ++control.source_value().degree;
            }
            else if(control.source_value().order > 0)
            {
                uint32_t highest =
                    control.aggregated().order -
                    control.source_value().degree;

                uint32_t shift = 2 + highest * control.source_value().i++;
                control.send(control.source_value().order + shift );

                if(control.source_value().i > control.source_value().degree)
                    control.source_value().i = 0;
            }
        },
        // gather
        [](auto&& control) {

            auto& msgs = control.messages();

            if(control.value().order == 0 && !msgs.empty())
                control.value().order = *std::max_element(msgs.begin(), msgs.end());
            else
                control.vote_to_halt();
        },
        // default state
        value{},
        // aggregator
        [](auto& states) {
            value aggregated;

            aggregated.order = std::count_if(
                states.begin(), states.end(),
                [](auto& state) {
                    return state.order == 0;
                });

            return aggregated;
        },
        // default aggregated
        default_agg));

    std::vector<uint32_t> inds(order.size());
    std::iota(inds.begin(), inds.end(), 0);

    std::sort(
        inds.begin(), inds.end(),
        [&order] (uint32_t lhs, uint32_t rhs) {
            return order[lhs].order < order[rhs].order;
        });

    for(auto i : inds)
    {
        if(order[i].order > 0)
        {
            ++_count;
            co_yield i;
        }
    }
}

template <typename Graph>
size_t ecsg_dfs<Graph>::count() const
{
    return _count;
}

template <typename Graph>
bool ecsg_dfs<Graph>::visited(uint32_t v) const
{
    return _visited[v];
}
