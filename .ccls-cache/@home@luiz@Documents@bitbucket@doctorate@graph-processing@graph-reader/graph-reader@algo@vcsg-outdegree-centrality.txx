#include <graph-reader/algo/vertex_centric_engine.hxx>
#include <cppcoro/sync_wait.hpp>
#include <algorithm>

template <typename Graph>
vcsg_outdegree_centrality<Graph>::vcsg_outdegree_centrality(const Graph& graph)
{
    vertex_centric_engine engine(graph);

    _ranks = cppcoro::sync_wait(engine.template run_scatter_gather<float, float>(
        // scather
        [](auto&& control) {

        },
        // gather
        [](auto&& control) {

            control.value() = control.out_degree();
            control.vote_to_halt();
        },
        //default_value
        0.f));
}


template <typename Graph>
cppcoro::generator<typename vcsg_outdegree_centrality<Graph>::item> vcsg_outdegree_centrality<Graph>::traverse()
{
    for(size_t i = 0; i < _ranks.size(); ++i)
        co_yield { i, _ranks[i] };
}
