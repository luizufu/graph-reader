#include <graph-reader/algo/vertex_centric_engine.hxx>
#include <cppcoro/sync_wait.hpp>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <mutex>

template <typename Graph>
vcsg_spsp<Graph>::vcsg_spsp(const Graph& graph, uint32_t s, uint32_t t)
    : _t(t)
{
    vertex_centric_engine engine(graph);

    dist_prev default_value;
    default_value.dist = std::numeric_limits<float>::max();
    default_value.prev = 0;

    _info = cppcoro::sync_wait(engine.template run_scatter_gather<dist_prev, dist_prev>(
        // scatter
        [s, t = _t](auto&& control) {

            // initialization step
            if(control.superstep() == 0 && control.id() == s)
                control.value() = { 0.f, control.id() };

            if(control.value().dist < std::numeric_limits<float>::max())
            {
                if(control.id() != t)
                {
                    dist_prev msg;
                    msg.prev = control.id();

                    for(auto n : control.out_neighbours())
                    {
                        msg.dist = control.value().dist + n.weight();
                        control.send_to(n.v, msg);
                    }
                }
            }
        },
        // gather
        [](auto&& control) {

            auto& msgs = control.messages();
            auto it = std::min_element(
                msgs.begin(), msgs.end(),
                [] (auto& lhs, auto& rhs) {
                    return lhs.dist < rhs.dist;
                });

            if(it != msgs.end() && it->dist < control.value().dist)
                control.value() = *it;
            else
                control.vote_to_halt();
        },
        // default state (uniform distribution)
        default_value));
}

template <typename Graph>
cppcoro::generator<typename vcsg_spsp<Graph>::path_item_t> vcsg_spsp<Graph>::traverse()
{
    return has_path_to(_t) ? path_to(_t) : cppcoro::generator<path_item_t>();
}


template <typename Graph>
bool vcsg_spsp<Graph>::has_path_to(uint32_t v) const
{
    return _info[v].dist < std::numeric_limits<float>::max()
        && _info[v].prev != v;
}

template <typename Graph>
float vcsg_spsp<Graph>::dist_to(uint32_t v) const
{
    return _info[v].dist;
}

template <typename Graph>
cppcoro::generator<typename vcsg_spsp<Graph>::path_item_t> vcsg_spsp<Graph>::path_to(uint32_t v) const
{
    co_yield { v, _info[v].dist };

    while(_info[v].prev != v)
    {
        v = _info[v].prev;
        co_yield { v , _info[v].dist };
    }
}
