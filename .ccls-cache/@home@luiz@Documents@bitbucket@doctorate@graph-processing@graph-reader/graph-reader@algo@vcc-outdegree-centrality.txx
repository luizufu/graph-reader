#include <graph-reader/algo/vertex_centric_engine.hxx>
#include <cppcoro/sync_wait.hpp>
#include <algorithm>

template <typename Graph>
vcc_outdegree_centrality<Graph>::vcc_outdegree_centrality(const Graph& graph)
{
    vertex_centric_engine engine(graph);

    _ranks = cppcoro::sync_wait(engine.template run_compute<float>(
        // compute
        [](auto&& control) {

            control.set_value(control.out_degree());
            control.vote_to_halt();
        },
        //default_value
        0.f));
}


template <typename Graph>
cppcoro::generator<typename vcc_outdegree_centrality<Graph>::item> vcc_outdegree_centrality<Graph>::traverse()
{
    for(size_t i = 0; i < _ranks.size(); ++i)
        co_yield { i, _ranks[i] };
}
