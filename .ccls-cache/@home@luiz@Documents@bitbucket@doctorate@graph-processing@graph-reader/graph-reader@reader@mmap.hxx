#pragma once

#include <cstdint>
#include <string>

struct mmapping
{
    int fd;
    char* begin;
    char* end;
};

mmapping mmap_file(const std::string& filename);
void unmmap_file(mmapping mapping);
