#pragma once

#include <graph-reader/aux/fixed_matrix.hxx>
#include <vector>
#include <queue>
#include <cppcoro/generator.hpp>

template <typename Graph>
class seq_apsp
{
    fixed_matrix<float> _dist;
    fixed_matrix<uint32_t> _prev;

    using path_item_t = std::pair<uint32_t, float>;

public:

    seq_apsp(const Graph& graph);

    cppcoro::generator<std::vector<path_item_t>> traverse();

    bool has_path(uint32_t u, uint32_t v) const;
    float dist(uint32_t u, uint32_t v) const;

    cppcoro::generator<path_item_t> path(uint32_t u, uint32_t v) const;


};


