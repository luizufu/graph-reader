#pragma once

#include <cppcoro/generator.hpp>
#include <vector>

template <typename Graph>
class vcsg_indegree_centrality
{

    std::vector<float> _ranks;

    using item = std::pair<uint32_t, float>;

public:

    vcsg_indegree_centrality(const Graph& graph);

    cppcoro::generator<item> traverse();
};

