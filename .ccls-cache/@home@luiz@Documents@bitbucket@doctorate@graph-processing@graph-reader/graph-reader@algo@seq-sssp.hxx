#pragma once

#include <vector>
#include <queue>
#include <cppcoro/generator.hpp>

template <typename Graph>
class seq_sssp
{
    std::vector<float> _dist;
    std::vector<uint32_t> _prev;

    using path_item_t = std::pair<uint32_t, float>;

public:

    seq_sssp(const Graph& graph, uint32_t s);

    cppcoro::generator<std::vector<path_item_t>> traverse();

    bool has_path_to(uint32_t v) const;
    float dist_to(uint32_t v) const;
    cppcoro::generator<path_item_t> path_to(uint32_t v) const;


};


