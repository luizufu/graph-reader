#pragma once

#include <cppcoro/generator.hpp>
#include <vector>

template <typename Graph>
class ecsg_pagerank
{
    std::vector<float> _ranks;

public:

    using item = std::pair<uint32_t, float>;

    ecsg_pagerank(const Graph& graph, size_t iterations = 30, float dump = .85f);

    cppcoro::generator<item> traverse();
};

