#include <graph-reader/algo/ecsg-sssp.hxx>
#include <limits>
#include <numeric>
#include <queue>
#include <stack>

template <typename Graph>
ecsg_closeness_centrality<Graph>::ecsg_closeness_centrality(const Graph& graph)
    : _ranks(graph.n_vertices(), 0)
{
    size_t n_vertices = _ranks.size();
    for(size_t u = 0; u < n_vertices; ++u)
    {
        ecsg_sssp search(graph, u);

        for(size_t v = 0; v < n_vertices; ++v)
            if(u != v && search.has_path_to(v))
                _ranks[u] += search.dist_to(v);

        _ranks[u] /= n_vertices - 1;
    }
}

template <typename Graph>
cppcoro::generator<typename ecsg_closeness_centrality<Graph>::item> ecsg_closeness_centrality<Graph>::traverse()
{
    for(size_t i = 0; i < _ranks.size(); ++i)
        co_yield { i, _ranks[i] };
}

