#pragma once

#include <graph-reader/algo/vertex-centric-engine.hxx>
#include <cppcoro/generator.hpp>

template <typename Graph>
class vcsg_cc
{
    vertex_centric_engine<Graph> _engine;
    /* std::vector<uint8_t> _ids; */
    /* std::vector<size_t> _sizes; */
    size_t _count = 0;

public:

    using item = std::pair<uint32_t, uint8_t>;

    vcsg_cc(const Graph& graph);

    cppcoro::generator<item> traverse();

    /* uint8_t id(uint32_t u) const; */
    /* size_t size(uint32_t u) const; */
    size_t count() const;
};

