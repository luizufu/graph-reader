#include <graph-reader/algo/vertex_centric_engine.hxx>
#include <cppcoro/sync_wait.hpp>
#include <algorithm>
#include <unordered_map>
#include <iostream>
#include <numeric>
#include <mutex>

template <typename Graph>
vcsg_apsp<Graph>::vcsg_apsp(const Graph& graph)
    /* : _info(graph.n_vertices()) */
{
    vertex_centric_engine engine(graph);

    size_t n_vertices = graph.n_vertices();

    using message_t = std::pair<uint32_t, dist_prev>;

    _info = cppcoro::sync_wait(engine.template run_scatter_gather<std::vector<dist_prev>, message_t>(
        // scatter
        [](auto&& control) {

            if(control.superstep() == 0)
            {
                control.value()[control.id()].dist = 0.f;
                control.value()[control.id()].prev = control.id();

                message_t msg;
                msg.first = control.id();
                msg.second.prev = control.id();

                for(auto n : control.out_neighbours())
                {
                    msg.second.dist =  n.weight();
                    control.send_to(n.v, msg);
                }
            }
            else
            {
                message_t msg;
                msg.second.prev = control.id();

                for(size_t s = 0; s < control.value().size(); ++s)
                {
                    for(auto n : control.out_neighbours())
                    {
                        float neigh_dist = control.value()[s].dist + n.weight();

                        if(neigh_dist < control.neighbour_value(n.v)[s].dist)
                        {
                            msg.first = s;
                            msg.second.dist = neigh_dist;

                            control.send_to(n.v, msg);
                        }
                    }
                }
            }
        },
        // gather
        [](auto&& control) {

            std::unordered_map<uint32_t, dist_prev> agg_map;
            for(auto& msg : control.messages())
                if(msg.second.dist < agg_map[msg.first].dist)
                    agg_map[msg.first] = msg.second;

            bool updated = false;
            for(auto [s, info] : agg_map)
            {
                if(info.dist < control.value()[s].dist)
                {
                    control.value()[s] = info;
                    updated = true;
                }
            }

            if(!updated)
                control.vote_to_halt();
        },
        // default state
        std::vector<dist_prev>(n_vertices)));
}

template <typename Graph>
cppcoro::generator<std::vector<typename vcsg_apsp<Graph>::path_item_t>> vcsg_apsp<Graph>::traverse()
{
    size_t n_vertices = _info.size();

    for(uint32_t i = 0; i < n_vertices; ++i)
    {
        for(uint32_t j = 0; j < n_vertices; ++j)
        {
            if(has_path(i, j))
            {
                std::vector<path_item_t> items;
                for(auto item : path(i, j))
                    items.push_back(item);

                co_yield items;
            }
        }
    }
}


template <typename Graph>
bool vcsg_apsp<Graph>::has_path(uint32_t u, uint32_t v) const
{
    return _info[v][u].dist < std::numeric_limits<float>::max()
        && _info[v][u].prev != v;
}

template <typename Graph>
float vcsg_apsp<Graph>::dist(uint32_t u, uint32_t v) const
{
    return _info[v][u].dist;
}

template <typename Graph>
cppcoro::generator<typename vcsg_apsp<Graph>::path_item_t> vcsg_apsp<Graph>::path(uint32_t u, uint32_t v) const
{
    co_yield { v, _info[v][u].dist };

    while(_info[v][u].prev != v)
    {
        v = _info[v][u].prev;
        co_yield { v , _info[v][u].dist };
    }
}
