template <typename Graph>
seq_dfs<Graph>::seq_dfs(const Graph& graph, uint32_t s)
    : _graph(graph)
    , _visited(graph.n_vertices(), false)
{
    _stack.push(s);
}

template <typename Graph>
cppcoro::generator<uint32_t> seq_dfs<Graph>::traverse()
{
    while(!_stack.empty())
    {
        uint32_t u = _stack.top(); _stack.pop();
        _visited[u] = true;
        ++_count;

        co_yield u;

        std::stack<uint32_t> reverse;
        for(auto n : _graph.out_neighbours(u))
            reverse.push(n.v);

        while(!reverse.empty())
        {
            uint32_t v = reverse.top(); reverse.pop();
            if(!_visited[v])
                _stack.push(v);
        }
    }
}

template <typename Graph>
size_t seq_dfs<Graph>::count() const
{
    return _count;
}

template <typename Graph>
bool seq_dfs<Graph>::visited(uint32_t v) const
{
    return _visited[v];
}

