#include <graph-reader/algo/vertex_centric_engine.hxx>
#include <cppcoro/sync_wait.hpp>
#include <algorithm>

template <typename Graph>
vcsg_indegree_centrality<Graph>::vcsg_indegree_centrality(const Graph& graph)
{
    vertex_centric_engine engine(graph);

    _ranks = cppcoro::sync_wait(engine.template run_scatter_gather<float, float>(
        // scatter
        [](auto&& control) {

            for(auto n : control.out_neighbours())
                control.send_to(n.v, 1.f);
        },
        // gather
        [](auto&& control) {

            auto& msgs = control.messages();
            float indegree= std::accumulate(msgs.begin(), msgs.end(), 0.f);
            control.set_value(indegree);

            control.vote_to_halt();
        },
        //default_value
        0.f));
}


template <typename Graph>
cppcoro::generator<typename vcsg_indegree_centrality<Graph>::item> vcsg_indegree_centrality<Graph>::traverse()
{
    for(size_t i = 0; i < _ranks.size(); ++i)
        co_yield { i, _ranks[i] };
}
