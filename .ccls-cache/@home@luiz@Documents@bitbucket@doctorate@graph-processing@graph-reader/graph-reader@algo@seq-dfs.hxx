#pragma once

#include <vector>
#include <stack>
#include <cppcoro/generator.hpp>

template <typename Graph>
class seq_dfs
{
    const Graph& _graph;
    std::vector<bool> _visited;
    std::stack<uint32_t> _stack;
    size_t _count = 0;

public:

    seq_dfs(const Graph& graph, uint32_t s);

    cppcoro::generator<uint32_t> traverse();

    size_t count() const;
    bool visited(uint32_t v) const;
};

