#pragma once

#include <vector>
#include <queue>
#include <cppcoro/generator.hpp>

template <typename Graph>
class seq_bfs
{
    const Graph& _graph;
    std::vector<bool> _visited;
    std::queue<uint32_t> _queue;
    size_t _count = 0;

public:

    seq_bfs(const Graph& graph, uint32_t s);

    cppcoro::generator<uint32_t> traverse();

    size_t count() const;
    bool visited(uint32_t v) const;
};
