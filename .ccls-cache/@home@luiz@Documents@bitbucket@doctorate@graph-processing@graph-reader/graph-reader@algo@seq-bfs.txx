template <typename Graph>
seq_bfs<Graph>::seq_bfs(const Graph& graph, uint32_t s)
    : _graph(graph)
    , _visited(graph.n_vertices(), false)
{
    _queue.push(s);
}

template <typename Graph>
cppcoro::generator<uint32_t> seq_bfs<Graph>::traverse()
{
    while(!_queue.empty())
    {
        uint32_t u = _queue.front(); _queue.pop();
        _visited[u] = true;
        ++_count;

        co_yield u;

        for(auto n : _graph.out_neighbours(u))
            if(!_visited[n.v])
                _queue.push(n.v);
    }
}

template <typename Graph>
size_t seq_bfs<Graph>::count() const
{
    return _count;
}

template <typename Graph>
bool seq_bfs<Graph>::visited(uint32_t v) const
{
    return _visited[v];
}

