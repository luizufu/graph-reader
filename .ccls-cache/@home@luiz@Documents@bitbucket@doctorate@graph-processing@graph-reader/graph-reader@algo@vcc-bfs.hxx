#pragma once

#include <graph-reader/algo/vertex-centric-engine.hxx>
#include <cppcoro/generator.hpp>
#include <vector>
#include <stack>

template <typename Graph>
class vcc_bfs
{
    vertex_centric_engine<Graph> _engine;
    uint32_t _s;
    std::vector<bool> _visited;
    size_t _count = 0;

public:

    vcc_bfs(const Graph& graph, uint32_t s);

    cppcoro::generator<uint32_t> traverse();

    size_t count() const;
    bool visited(uint32_t v) const;
};

