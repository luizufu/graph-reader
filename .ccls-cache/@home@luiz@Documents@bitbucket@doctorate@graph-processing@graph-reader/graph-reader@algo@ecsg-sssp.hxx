#pragma once

#include <cppcoro/generator.hpp>
#include <vector>
#include <limits>

template <typename Graph>
class ecsg_sssp
{
    struct dist_prev
    {
        float dist = std::numeric_limits<float>::max();
        uint32_t prev;
    };

    std::vector<dist_prev> _info;

    using path_item_t = std::pair<uint32_t, float>;

public:

    ecsg_sssp(const Graph& graph, uint32_t s);

    cppcoro::generator<std::vector<path_item_t>> traverse();

    bool has_path_to(uint32_t v) const;
    float dist_to(uint32_t v) const;
    cppcoro::generator<path_item_t> path_to(uint32_t v) const;
};

