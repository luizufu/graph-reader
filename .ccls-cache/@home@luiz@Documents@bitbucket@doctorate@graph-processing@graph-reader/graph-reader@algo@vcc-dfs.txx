#include <cppcoro/sync_wait.hpp>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <mutex>

template <typename Graph>
vcc_dfs<Graph>::vcc_dfs(const Graph& graph, uint32_t s)
    : _engine(graph)
    , _graph(graph)
    , _s(s)
{
}


template <typename Graph>
cppcoro::generator<uint32_t> vcc_dfs<Graph>::traverse()
{
    auto order = cppcoro::sync_wait(_engine.template run_compute<uint32_t, uint32_t>(
        // compute
        [s = _s](auto&& control) {

            // if not visited yet
            if(control.value() == 0)
            {
                auto& msgs = control.messages();

                // if it's the source vertex at first iteration or
                // it's some vertex that received a new message
                if((control.superstep() == 0 && control.id() == s)
                    || !msgs.empty())
                {
                    // set my order to 1 + max element in messages
                    uint32_t my_order = 1;
                    if(!msgs.empty())
                        my_order+= *std::max_element(msgs.begin(), msgs.end());

                    control.value() = my_order;

                    // send orders to my neighbours that are proportional
                    // to the highest possible number a path could take
                    // aggregated stores the number of vertices not visited yet
                    size_t i = 0;
                    uint32_t highest =
                        control.aggregated() -
                        control.out_degree();

                    for(auto n : control.out_neighbours())
                        control.send_to(n.v, my_order + 2 + highest * i++);
                }
            }

            // i'm done
            control.vote_to_halt();
        },
        // default state
        0,
        // aggregator
        [](auto& states) {
            return std::count_if(
                states.begin(), states.end(),
                [](uint32_t state) {
                    return state == 0;
                });
        },
        // default aggregated
        _graph.n_vertices()));

    std::vector<uint32_t> inds(order.size());
    std::iota(inds.begin(), inds.end(), 0);

    std::sort(
        inds.begin(), inds.end(),
        [&order] (uint32_t lhs, uint32_t rhs) {
            return order[lhs] < order[rhs];
        });

    for(auto i : inds)
    {
        if(order[i] > 0)
        {
            ++_count;
            co_yield i;
        }
    }
}

template <typename Graph>
size_t vcc_dfs<Graph>::count() const
{
    return _count;
}

template <typename Graph>
bool vcc_dfs<Graph>::visited(uint32_t v) const
{
    return _visited[v];
}
