#include <cppcoro/sync_wait.hpp>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <mutex>

template <typename Graph>
ecsg_bfs<Graph>::ecsg_bfs(const Graph& graph, uint32_t s)
    : _engine(graph)
    , _s(s)
    , _visited(graph.n_vertices())
{
}


template <typename Graph>
cppcoro::generator<uint32_t> ecsg_bfs<Graph>::traverse()
{
    auto order = cppcoro::sync_wait(_engine.template run_scatter_gather<uint32_t, uint32_t>(
        // scatter
        [s = _s](auto&& control) {

            if(control.source_id() == s && control.superstep() == 0)
                control.source_value() = 1;

            if(control.source_value() > 0)
                control.send(control.source_value() + 1);
        },
        // gather
        [](auto&& control) {

            auto& msgs = control.messages();

            if(control.value() == 0 && !msgs.empty())
                control.value() = *std::max_element(msgs.begin(), msgs.end());
            else
                control.vote_to_halt();
        },
        //default_value
        0));

    std::vector<uint32_t> inds(order.size());
    std::iota(inds.begin(), inds.end(), 0);

    std::sort(
        inds.begin(), inds.end(),
        [&order] (uint32_t lhs, uint32_t rhs) {
            return order[lhs] < order[rhs];
        });

    for(auto i : inds)
    {
        if(order[i] > 0)
        {
            _visited[i] = true;
            ++_count;
            co_yield i;
        }
    }
}

template <typename Graph>
size_t ecsg_bfs<Graph>::count() const
{
    return _count;
}

template <typename Graph>
bool ecsg_bfs<Graph>::visited(uint32_t v) const
{
    return _visited[v];
}
