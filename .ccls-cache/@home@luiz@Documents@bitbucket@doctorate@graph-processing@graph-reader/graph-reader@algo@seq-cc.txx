#include <iostream>
#include <stack>

template <typename Graph>
seq_cc<Graph>::seq_cc(const Graph& graph)
    : _graph(graph)
    , _visited(graph.n_vertices(), false)
    /* , _ids(graph.n_vertices()) */
{
}

template <typename Graph>
cppcoro::generator<typename seq_cc<Graph>::item> seq_cc<Graph>::traverse()
{
    std::stack<uint32_t> stack;

    for(size_t s = 0; s < _graph.n_vertices(); ++s)
    {
        if(!_visited[s])
        {
            stack.push(s);
            /* _sizes.push_back(0); */

            while(!stack.empty())
            {
                uint32_t u = stack.top(); stack.pop();
                _visited[u] = true;
                /* _ids[u] = _count; */
                /* ++_sizes[_count]; */

                co_yield { u, _count };

                for(auto n : _graph.out_neighbours(u))
                    if(!_visited[n.v])
                        stack.push(n.v);
            }

            ++_count;
        }
    }
}

/* template <typename Graph> */
/* uint8_t seq_cc<Graph>::id(uint32_t u) const */
/* { */
/*     return _ids[u]; */
/* } */

/* template <typename Graph> */
/* size_t seq_cc<Graph>::size(uint32_t u) const */
/* { */
/*     return _sizes[_ids[u]]; */
/* } */

template <typename Graph>
size_t seq_cc<Graph>::count() const
{
}
