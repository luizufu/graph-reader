#pragma once

#include <cppcoro/generator.hpp>
#include <vector>
#include <limits>

template <typename Graph>
class vcsg_betweenness_centrality
{
    std::vector<float> _ranks;

    using item = std::pair<uint32_t, float>;

public:

    vcsg_betweenness_centrality(const Graph& graph);

    cppcoro::generator<item> traverse();
};

