#pragma once

#include <cppcoro/generator.hpp>
#include <vector>

template <typename Graph>
class seq_outdegree_centrality
{
    std::vector<float> _ranks;

    using item = std::pair<uint32_t, float>;

public:

    seq_outdegree_centrality(const Graph& graph);

    cppcoro::generator<item> traverse();
};

