#pragma once

#include <string>
#include <cstdint>

#include <libgraph-processing/utility.hxx>


void run_edge_list_ecc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v, const stg::graph_header& props);

void run_adj_list_ecc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v, const graph_props& props);

void run_edge_grid_ecc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v, const graph_props& props);

void run_weighted_edge_list_ecc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v, const graph_props& props);

void run_weighted_adj_list_ecc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v, const graph_props& props);

void run_weighted_edge_grid_ecc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v, const graph_props& props);
