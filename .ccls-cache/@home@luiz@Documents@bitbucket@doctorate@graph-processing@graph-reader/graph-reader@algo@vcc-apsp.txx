#include <graph-reader/algo/vertex_centric_engine.hxx>
#include <cppcoro/sync_wait.hpp>
#include <algorithm>
#include <unordered_map>
#include <iostream>
#include <numeric>
#include <mutex>

template <typename Graph>
vcc_apsp<Graph>::vcc_apsp(const Graph& graph)
    /* : _info(graph.n_vertices()) */
{
    vertex_centric_engine engine(graph);

    size_t n_vertices = graph.n_vertices();

    /* dist_prev default_value; */
    /* default_value.dist = std::numeric_limits<float>::max(); */
    /* default_value.prev = 0; */

    /* for(size_t s = 0; s < n_vertices; ++s) */
    /* { */
        /* _info[s] = cppcoro::sync_wait(engine.template run<dist_prev>( */
        /*     // compute */
        /*     [this, s](auto&& control) { */

        /*         auto& msgs = control.messages(); */

        /*         // if i'm the source than my_dist is 0 and I point to myself */
        /*         dist_prev my_info; */
        /*         my_info.dist = control.id() == s ? */
        /*             0 : std::numeric_limits<float>::max(); */
        /*         my_info.prev = control.id(); */


        /*         // update my_dist and my_prev looking at the messages that come */
        /*         if(!msgs.empty()) */
        /*         { */
        /*             for(auto& msg : msgs) */
        /*                 if(msg.dist < my_info.dist) */
        /*                     my_info = msg; */
        /*         } */

        /*         // if my_dist is lower than what I had */
        /*         if(my_info.dist < control.value().dist) */
        /*         { */
        /*             // update what I had */
        /*             control.set_value(my_info); */
        /*             float w = 1.f; // change this if weighted */

        /*             // prepare msg to my neighbours */
        /*             dist_prev neigh_info; */
        /*             neigh_info.dist = my_info.dist + w; */
        /*             neigh_info.prev = control.id(); */

        /*             // send a msg to my neighbours telling their new distance */
        /*             for(auto neigh : control.out_neighbours()) */
        /*                 control.send_to(neigh, neigh_info); */
        /*         } */

        /*         control.vote_to_halt(); */
        /*     }, */
        /*     // default state (uniform distribution) */
        /*     default_value)); */
    /* } */


    using message_t = std::pair<uint32_t, dist_prev>;

    _info = cppcoro::sync_wait(engine.template run_compute<std::vector<dist_prev>, message_t>(
        // compute
        [](auto&& control) {

            // send message to my neighbours with source (msg.first) being me
            if(control.superstep() == 0)
            {
                // set my initial data
                control.value()[control.id()].dist = 0.f;
                control.value()[control.id()].prev = control.id();

                // prepare and send message to my neighbours
                for(auto n : control.out_neighbours())
                {
                    message_t msg;
                    msg.first = control.id();
                    msg.second.dist = n.weight();
                    msg.second.prev = control.id();

                    control.send_to(n.v, msg);
                }
            }
            else
            {
                // aggregate messages by source using min()
                std::unordered_map<uint32_t, dist_prev> agg_map;
                for(auto& msg : control.messages())
                    if(msg.second.dist < agg_map[msg.first].dist)
                        agg_map[msg.first] = msg.second;

                // for each source message with minimal distance
                for(auto [s, info] : agg_map)
                {
                    // send message to my neighbours with source (msg.first)
                    // being s whether message dist is lesser than what I know
                    if(info.dist < control.value()[s].dist)
                    {
                        // update what I had
                        control.value()[s] = info;

                        // prepare and send msg to my neighbours
                        for(auto n : control.out_neighbours())
                        {
                            message_t msg;
                            msg.first = s;
                            msg.second.dist = info.dist + n.weight();
                            msg.second.prev = control.id();

                            control.send_to(n.v, msg);
                        }
                    }
                }
            }

            // i'm done
            control.vote_to_halt();
        },
        // default state
        std::vector<dist_prev>(n_vertices)));
}

template <typename Graph>
cppcoro::generator<std::vector<typename vcc_apsp<Graph>::path_item_t>> vcc_apsp<Graph>::traverse()
{
    size_t n_vertices = _info.size();

    for(uint32_t i = 0; i < n_vertices; ++i)
    {
        for(uint32_t j = 0; j < n_vertices; ++j)
        {
            if(has_path(i, j))
            {
                std::vector<path_item_t> items;
                for(auto item : path(i, j))
                    items.push_back(item);

                co_yield items;
            }
        }
    }
}


template <typename Graph>
bool vcc_apsp<Graph>::has_path(uint32_t u, uint32_t v) const
{
    return _info[v][u].dist < std::numeric_limits<float>::max()
        && _info[v][u].prev != v;
}

template <typename Graph>
float vcc_apsp<Graph>::dist(uint32_t u, uint32_t v) const
{
    return _info[v][u].dist;
}

template <typename Graph>
cppcoro::generator<typename vcc_apsp<Graph>::path_item_t> vcc_apsp<Graph>::path(uint32_t u, uint32_t v) const
{
    co_yield { v, _info[v][u].dist };

    while(_info[v][u].prev != v)
    {
        v = _info[v][u].prev;
        co_yield { v , _info[v][u].dist };
    }
}
