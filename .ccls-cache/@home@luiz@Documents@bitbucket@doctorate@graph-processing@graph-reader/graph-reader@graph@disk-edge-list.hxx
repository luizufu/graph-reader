#pragma once

#include <graph-reader/reader/mmap.hxx>
#include <cppcoro/generator.hpp>
#include <cstdint>
#include <iterator>

class disk_edge_list
{
    mmapping _index_mapping;
    mmapping _edges_mapping;

public:

    using edge = std::pair<uint32_t, uint32_t>;

    disk_edge_list(const std::string& in);
    ~disk_edge_list();

    bool has_edge(uint32_t u, uint32_t v) const;

    cppcoro::generator<uint32_t> out_neighbours(uint32_t u) const;
    cppcoro::generator<uint32_t> in_neighbours(uint32_t v) const;
    cppcoro::generator<edge> edges() const;

    size_t out_degree(uint32_t u) const;
    size_t in_degree(uint32_t v) const;

    size_t n_vertices() const;
    size_t n_edges() const;
};
