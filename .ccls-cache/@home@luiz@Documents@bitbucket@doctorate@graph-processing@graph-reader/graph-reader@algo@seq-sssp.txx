#include <limits>
#include <numeric>

template <typename Graph>
seq_sssp<Graph>::seq_sssp(const Graph& graph, uint32_t s)
    : _dist(graph.n_vertices(), std::numeric_limits<float>::max())
    , _prev(_dist.size())
{
    size_t n_vertices = _prev.size();

    _dist[s] = 0.f;
    std::iota(_prev.begin(), _prev.end(), 0);

    // for min heap
    auto comparator = [](const path_item_t& lhs, const path_item_t& rhs) {
        return lhs.second > rhs.second;
    };

    std::priority_queue<
        path_item_t,
        std::vector<path_item_t>,
        decltype(comparator)> queue(comparator);

    queue.push({ s, 0.f });
    while(!queue.empty())
    {
        uint32_t u = queue.top().first; queue.pop();

        for(auto n : graph.out_neighbours(u))
        {
            if(_dist[u] + n.weight() < _dist[n.v])
            {
                _dist[n.v] = _dist[u] + n.weight();
                _prev[n.v] = u;
                queue.push({ n.v, _dist[n.v] });
            }
       }
    }
}

template <typename Graph>
cppcoro::generator<std::vector<typename seq_sssp<Graph>::path_item_t>> seq_sssp<Graph>::traverse()
{
    size_t n_vertices = _prev.size();

    for(uint32_t i = 0; i < n_vertices; ++i)
    {
        if(has_path_to(i))
        {
            std::vector<path_item_t> items;
            for(auto item : path_to(i))
                items.push_back(item);

            co_yield items;
        }
    }
}


template <typename Graph>
bool seq_sssp<Graph>::has_path_to(uint32_t v) const
{
    return _dist[v] < std::numeric_limits<float>::max() && _prev[v] != v;
}

template <typename Graph>
float seq_sssp<Graph>::dist_to(uint32_t v) const
{
    return _dist[v];
}

template <typename Graph>
cppcoro::generator<typename seq_sssp<Graph>::path_item_t> seq_sssp<Graph>::path_to(uint32_t v) const
{
    co_yield { v, _dist[v] };

    while(_prev[v] != v)
    {
        v = _prev[v];
        co_yield { v , _dist[v] };
    }
}
