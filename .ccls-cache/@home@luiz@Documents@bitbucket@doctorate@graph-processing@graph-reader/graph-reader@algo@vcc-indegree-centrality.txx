#include <graph-reader/algo/vertex_centric_engine.hxx>
#include <cppcoro/sync_wait.hpp>
#include <algorithm>

template <typename Graph>
vcc_indegree_centrality<Graph>::vcc_indegree_centrality(const Graph& graph)
{
    vertex_centric_engine engine(graph);

    _ranks = cppcoro::sync_wait(engine.template run_compute<float, float>(
        // compute
        [](auto&& control) {

            if(control.superstep() == 0)
            {
                for(auto n : control.out_neighbours())
                    control.send_to(n.v, 1.f);
            }
            else
            {
                auto& msgs = control.messages();
                float indegree= std::accumulate(msgs.begin(), msgs.end(), 0.f);
                control.set_value(indegree);
            }

            control.vote_to_halt();
        },
        //default_value
        0.f));
}


template <typename Graph>
cppcoro::generator<typename vcc_indegree_centrality<Graph>::item> vcc_indegree_centrality<Graph>::traverse()
{
    for(size_t i = 0; i < _ranks.size(); ++i)
        co_yield { i, _ranks[i] };
}
