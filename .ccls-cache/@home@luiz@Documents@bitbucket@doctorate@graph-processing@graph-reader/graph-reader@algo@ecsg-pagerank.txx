#include <graph-reader/algo/edge_centric_engine.hxx>
#include <cppcoro/sync_wait.hpp>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <mutex>

template <typename Graph>
ecsg_pagerank<Graph>::ecsg_pagerank(const Graph& graph, size_t iterations, float dump)
    : _ranks(graph.n_vertices())
{
    edge_centric_engine engine(graph);
    size_t n_vertices = graph.n_vertices();

    struct value
    {
        float rank;
        size_t degree;
    };

    value default_value;
    default_value.rank = 1.f / n_vertices;
    default_value.degree = 0;


    auto info = cppcoro::sync_wait(engine.template run_scatter_gather<value, float>(
        // scatter
        [&](auto&& control) {

            if(control.superstep() == 0)
                ++control.source_value().degree;
            else
                control.send(control.source_value().rank /
                             control.source_value().degree);
        },
        // gather
        [&](auto&& control) {

            // lost 1 iteration computing degree
            if(control.superstep() < iterations + 1)
            {
                auto& msgs = control.messages();
                float sum = std::accumulate(msgs.begin(), msgs.end(), 0.f);

                control.value().rank=(1.f-dump)/control.n_vertices() + dump*sum;
            }
            else
            {
                control.vote_to_halt();
            }
        },
        // default state (uniform distribution)
        default_value));

    for(size_t i = 0; i < n_vertices; ++i)
        _ranks[i] = info[i].rank;
}


template <typename Graph>
cppcoro::generator<typename ecsg_pagerank<Graph>::item> ecsg_pagerank<Graph>::traverse()
{
    for(size_t i = 0; i < _ranks.size(); ++i)
        co_yield { i, _ranks[i] };
}
