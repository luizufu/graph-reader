#pragma once

#include <cppcoro/generator.hpp>
#include <vector>

template <typename Graph>
class seq_indegree_centrality
{
    std::vector<float> _ranks;

    using item = std::pair<uint32_t, float>;

public:

    seq_indegree_centrality(const Graph& graph);

    cppcoro::generator<item> traverse();
};

