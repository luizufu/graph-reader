#pragma once

#include <vector>
#include <cppcoro/generator.hpp>

template <typename Graph>
class seq_spsp
{
    std::vector<float> _dist;
    std::vector<uint32_t> _prev;
    uint32_t _t;

    using path_item_t = std::pair<uint32_t, float>;

public:

    seq_spsp(const Graph& graph, uint32_t s, uint32_t t);

    cppcoro::generator<path_item_t> traverse();

    bool has_path_to(uint32_t v) const;
    float dist_to(uint32_t v) const;
    cppcoro::generator<path_item_t> path_to(uint32_t v) const;
};


