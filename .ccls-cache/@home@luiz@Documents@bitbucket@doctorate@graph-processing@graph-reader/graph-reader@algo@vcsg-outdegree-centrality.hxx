#pragma once

#include <cppcoro/generator.hpp>
#include <vector>

template <typename Graph>
class vcsg_outdegree_centrality
{

    std::vector<float> _ranks;

    using item = std::pair<uint32_t, float>;

public:

    vcsg_outdegree_centrality(const Graph& graph);

    cppcoro::generator<item> traverse();
};

