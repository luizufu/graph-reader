#pragma once

#include <cppcoro/generator.hpp>
#include <vector>
#include <limits>

template <typename Graph>
class ecsg_apsp
{
    struct dist_prev
    {
        float dist = std::numeric_limits<float>::max();
        uint32_t prev;
    };

    //column-wise
    std::vector<std::vector<dist_prev>> _info;

    using path_item_t = std::pair<uint32_t, float>;

public:

    ecsg_apsp(const Graph& graph);

    cppcoro::generator<std::vector<path_item_t>> traverse();

    bool has_path(uint32_t u, uint32_t v) const;
    float dist(uint32_t u, uint32_t v) const;

    cppcoro::generator<path_item_t> path(uint32_t u, uint32_t v) const;
};
