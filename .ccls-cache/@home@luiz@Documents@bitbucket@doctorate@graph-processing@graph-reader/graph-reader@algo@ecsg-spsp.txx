#include <graph-reader/algo/edge_centric_engine.hxx>
#include <cppcoro/sync_wait.hpp>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <mutex>

template <typename Graph>
ecsg_spsp<Graph>::ecsg_spsp(const Graph& graph, uint32_t s, uint32_t t)
    : _t(t)
{
    edge_centric_engine engine(graph);

    dist_prev default_value;
    default_value.dist = std::numeric_limits<float>::max();
    default_value.prev = 0;

    _info = cppcoro::sync_wait(engine.template run_scatter_gather<dist_prev, dist_prev>(
        // scatter
        [s, t = _t](auto&& control) {

            // initialization step
            if(control.superstep() == 0 && control.source_id() == s)
                control.source_value() = { 0.f, control.source_id() };

            if(control.source_value().dist < std::numeric_limits<float>::max())
            {
                if(control.source_id() != t)
                {
                    dist_prev msg;
                    msg.dist = control.source_value().dist + control.weight();
                    msg.prev = control.source_id();

                    control.send(msg);
                }
            }
        },
        // gather
        [](auto&& control) {

            auto& msgs = control.messages();
            auto it = std::min_element(
                msgs.begin(), msgs.end(),
                [] (auto& lhs, auto& rhs) {
                    return lhs.dist < rhs.dist;
                });

            if(it != msgs.end() && it->dist < control.value().dist)
                control.value() = *it;
            else
                control.vote_to_halt();
        },
        // default state (uniform distribution)
        default_value));
}

template <typename Graph>
cppcoro::generator<typename ecsg_spsp<Graph>::path_item_t> ecsg_spsp<Graph>::traverse()
{
    return has_path_to(_t) ? path_to(_t) : cppcoro::generator<path_item_t>();
}


template <typename Graph>
bool ecsg_spsp<Graph>::has_path_to(uint32_t v) const
{
    return _info[v].dist < std::numeric_limits<float>::max()
        && _info[v].prev != v;
}

template <typename Graph>
float ecsg_spsp<Graph>::dist_to(uint32_t v) const
{
    return _info[v].dist;
}

template <typename Graph>
cppcoro::generator<typename ecsg_spsp<Graph>::path_item_t> ecsg_spsp<Graph>::path_to(uint32_t v) const
{
    co_yield { v, _info[v].dist };

    while(_info[v].prev != v)
    {
        v = _info[v].prev;
        co_yield { v , _info[v].dist };
    }
}
