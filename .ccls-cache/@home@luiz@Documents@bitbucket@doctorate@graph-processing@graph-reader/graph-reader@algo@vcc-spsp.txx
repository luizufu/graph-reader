#include <graph-reader/algo/vertex_centric_engine.hxx>
#include <cppcoro/sync_wait.hpp>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <mutex>

template <typename Graph>
vcc_spsp<Graph>::vcc_spsp(const Graph& graph, uint32_t s, uint32_t t)
    : _t(t)
{
    vertex_centric_engine engine(graph);

    dist_prev default_value;
    default_value.dist = std::numeric_limits<float>::max();
    default_value.prev = 0;

    _info = cppcoro::sync_wait(engine.template run_compute<dist_prev, dist_prev>(
        // compute
        [s, t = _t](auto&& control) {

            auto& msgs = control.messages();

            // if i'm the source than my_dist is 0 and I point to myself
            dist_prev my_info;
            my_info.dist = control.id() == s ?
                0 : std::numeric_limits<float>::max();
            my_info.prev = control.id();


            // update my_dist and my_prev looking at the messages that come
            for(auto& msg : msgs)
                if(msg.dist < my_info.dist)
                    my_info = msg;

            // if my_dist is lower than what I had
            if(my_info.dist < control.value().dist)
            {
                // update what I had
                control.set_value(my_info);

                // only send to neighbours if i'm not t
                if(control.id() != t)
                {
                    dist_prev msg;
                    msg.prev = control.id();

                    // send a msg to my neighbours telling they new distance
                    for(auto n : control.out_neighbours())
                    {
                        msg.dist = my_info.dist + n.weight();
                        control.send_to(n.v, msg);
                    }
                }
            }

            control.vote_to_halt();
        },
        // default state (uniform distribution)
        default_value));
}

template <typename Graph>
cppcoro::generator<typename vcc_spsp<Graph>::path_item_t> vcc_spsp<Graph>::traverse()
{
    return has_path_to(_t) ? path_to(_t) : cppcoro::generator<path_item_t>();
}


template <typename Graph>
bool vcc_spsp<Graph>::has_path_to(uint32_t v) const
{
    return _info[v].dist < std::numeric_limits<float>::max()
        && _info[v].prev != v;
}

template <typename Graph>
float vcc_spsp<Graph>::dist_to(uint32_t v) const
{
    return _info[v].dist;
}

template <typename Graph>
cppcoro::generator<typename vcc_spsp<Graph>::path_item_t> vcc_spsp<Graph>::path_to(uint32_t v) const
{
    co_yield { v, _info[v].dist };

    while(_info[v].prev != v)
    {
        v = _info[v].prev;
        co_yield { v , _info[v].dist };
    }
}
