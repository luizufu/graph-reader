#include <cppcoro/sync_wait.hpp>
#include <vector>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <mutex>

template <typename Graph>
ecsg_cc<Graph>::ecsg_cc(const Graph& graph)
    : _engine(graph)
{
}


template <typename Graph>
cppcoro::generator<typename ecsg_cc<Graph>::item> ecsg_cc<Graph>::traverse()
{
    auto colors = cppcoro::sync_wait(_engine.template run_scatter_gather<uint32_t, uint32_t>(
        // scatter
        [](auto&& control) {

            if(control.superstep() == 0)
                control.source_value() = control.source_id();

            control.send(control.source_value());
        },
        // gather
        [](auto&& control) {

            auto& msgs = control.messages();
            auto it = std::min_element(msgs.begin(), msgs.end());

            if(it != msgs.end() && *it < control.value())
                control.value() = *it;
            else
                control.vote_to_halt();
        },
        //default_value
        0));

    std::vector<uint32_t> inds(colors.size());
    std::iota(inds.begin(), inds.end(), 0);

    std::sort(
        inds.begin(), inds.end(),
        [&colors] (uint32_t lhs, uint32_t rhs) {
            return colors[lhs] < colors[rhs];
        });

    uint32_t last_color = colors[inds[0]];
    for(auto i : inds)
    {
        co_yield { i, colors[i] };

        if(colors[i] != last_color)
            ++_count;
    }
}

/* template <typename Graph> */
/* uint8_t seq_cc<Graph>::id(uint32_t u) const */
/* { */
/*     return _ids[u]; */
/* } */

/* template <typename Graph> */
/* size_t seq_cc<Graph>::size(uint32_t u) const */
/* { */
/*     return _sizes[_ids[u]]; */
/* } */

template <typename Graph>
size_t ecsg_cc<Graph>::count() const
{
    return _count;
}
