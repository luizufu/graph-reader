#pragma once

#include <graph-reader/algo/edge-centric-engine.hxx>
#include <cppcoro/generator.hpp>
#include <vector>
#include <stack>

template <typename Graph>
class ecsg_dfs
{
    edge_centric_engine<Graph> _engine;
    const Graph& _graph;
    uint32_t _s;
    std::vector<bool> _visited;
    size_t _count = 0;

public:

    ecsg_dfs(const Graph& graph, uint32_t s);

    cppcoro::generator<uint32_t> traverse();

    size_t count() const;
    bool visited(uint32_t v) const;
};

