#include <graph-reader/algo/edge_centric_engine.hxx>
#include <cppcoro/sync_wait.hpp>
#include <algorithm>

template <typename Graph>
ecsg_indegree_centrality<Graph>::ecsg_indegree_centrality(const Graph& graph)
{
    edge_centric_engine engine(graph);

    _ranks = cppcoro::sync_wait(engine.template run_scatter_gather<float, float>(
        // scatter
        [](auto&& control) {

            control.send(control.weight());
        },
        // gather
        [](auto&& control) {

            auto& msgs = control.messages();
            float indegree= std::accumulate(msgs.begin(), msgs.end(), 0.f);
            control.set_value(indegree);

            control.vote_to_halt();
        },
        //default_value
        0.f));
}


template <typename Graph>
cppcoro::generator<typename ecsg_indegree_centrality<Graph>::item> ecsg_indegree_centrality<Graph>::traverse()
{
    for(size_t i = 0; i < _ranks.size(); ++i)
        co_yield { i, _ranks[i] };
}
