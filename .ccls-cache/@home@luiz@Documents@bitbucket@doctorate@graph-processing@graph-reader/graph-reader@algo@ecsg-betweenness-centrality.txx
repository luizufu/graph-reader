#include <graph-reader/algo/vertex_centric_engine.hxx>
#include <graph-reader/aux/float-comparison.hxx>
#include <cppcoro/sync_wait.hpp>
#include <algorithm>
#include <unordered_map>
#include <iostream>
#include <numeric>
#include <mutex>

// TODO convert to edge centric (hard)
template <typename Graph>
ecsg_betweenness_centrality<Graph>::ecsg_betweenness_centrality(const Graph& graph)
    : _ranks(graph.n_vertices(), 0.f)
{
    vertex_centric_engine engine(graph);
    size_t n_vertices = graph.n_vertices();
    float infinity = std::numeric_limits<float>::max();

    struct value
    {
        float dist = std::numeric_limits<float>::max();
        std::vector<uint32_t> pred;
        float sigma = 1.f;
    };

    struct message
    {
        uint32_t sender;
        float dist = std::numeric_limits<float>::max();
        float sigma = 0.f;
    };

    for(size_t s = 0; s < n_vertices; ++s)
    {
        auto info = cppcoro::sync_wait(engine.template run_scatter_gather<value, message>(
            // scatter
            [s](auto&& control) {

                // initialization step
                if(control.superstep() == 0 && control.id() == s)
                    control.value().dist = 0.f;

                if(control.id() == s || control.superstep() > 0)
                {
                    message msg;
                    msg.sender = control.id();

                    for(auto n : control.out_neighbours())
                    {
                        msg.dist = control.value().dist + n.weight();
                        msg.sigma = n.weight() * control.value().sigma;

                        control.send_to(n.v, msg);
                    }
                }
            },
            // gather
            [](auto&& control) {

                auto& msgs = control.messages();

                if(msgs.empty())
                {
                    control.vote_to_halt();
                    return;
                }

                auto it = std::min_element(
                    msgs.begin(), msgs.end(),
                    [](auto& lhs, auto& rhs) {
                        return lhs.dist < rhs.dist;
                    });

                if(it->dist < control.value().dist)
                {
                    control.value().dist = it->dist;
                    control.value().sigma = 0.f;
                    control.value().pred.clear();
                }

                if(approximately_equal(it->dist, control.value().dist))
                {
                    control.value().sigma += it->sigma;
                    control.value().pred.push_back(it->sender);
                }
                else
                {
                    control.vote_to_halt();
                }
            },
            // default state
            value{}));

        float max_dist = 0.f;
        for(auto& item : info)
            if(item.dist < infinity && item.dist > max_dist)
                max_dist = item.dist;

        auto deltas = cppcoro::sync_wait(engine.template run_scatter_gather<float, float>(
            // scatter
            [&info, max_dist](auto&& control) {

                bool have_max_dist = approximately_equal(
                    info[control.id()].dist, max_dist);

                if((control.superstep() == 0 && have_max_dist) ||
                    control.superstep() > 0)
                {
                    size_t v = control.id();
                    for(auto u : info[v].pred)
                    {
                        float u_delta =
                            (info[u].sigma / info[v].sigma) *
                            (1 + control.value());

                        control.send_to(u, u_delta);
                    }

                    info[v].pred.clear();
                }
            },
            // gather
            [](auto&& control) {

                auto& msgs = control.messages();

                for(auto& msg : msgs)
                    control.value() += msg;

                if(msgs.empty())
                    control.vote_to_halt();
            },
            // default state
            0.f));

        // accumulate deltas related to paths starting from s
        for(uint32_t i = 0; i < n_vertices; ++i)
            if(i != s)
                _ranks[i] += deltas[i];
    }
}

template <typename Graph>
cppcoro::generator<typename ecsg_betweenness_centrality<Graph>::item> ecsg_betweenness_centrality<Graph>::traverse()
{
    for(size_t i = 0; i < _ranks.size(); ++i)
        co_yield { i, _ranks[i] };
}
