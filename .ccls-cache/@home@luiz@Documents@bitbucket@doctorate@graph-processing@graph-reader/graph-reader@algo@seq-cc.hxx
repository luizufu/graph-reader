#pragma once

#include <cppcoro/generator.hpp>
#include <vector>

template <typename Graph>
class seq_cc
{
    const Graph& _graph;
    std::vector<bool> _visited;
    /* std::vector<uint8_t> _ids; */
    /* std::vector<size_t> _sizes; */
    size_t _count = 0;

public:

    using item = std::pair<uint32_t, uint8_t>;

    seq_cc(const Graph& graph);

    cppcoro::generator<item> traverse();

    /* uint8_t id(uint32_t u) const; */
    /* size_t size(uint32_t u) const; */
    size_t count() const;
};

