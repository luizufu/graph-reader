#pragma once

#include <string>
#include <cstdint>


void run_edge_list_vcc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v);

void run_adj_list_vcc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v);

void run_edge_grid_vcc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v);

void run_weighted_edge_list_vcc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v);

void run_weighted_adj_list_vcc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v);

void run_weighted_edge_grid_vcc(const std::string& index, const std::string& query,
                      uint32_t u, uint32_t v);
