#pragma once

#include <vector>
#include <cppcoro/generator.hpp>

template <typename Graph>
class ecsg_closeness_centrality
{
    std::vector<float> _ranks;

    using item = std::pair<uint32_t, float>;

public:

    ecsg_closeness_centrality(const Graph& graph);

    cppcoro::generator<item> traverse();
};


