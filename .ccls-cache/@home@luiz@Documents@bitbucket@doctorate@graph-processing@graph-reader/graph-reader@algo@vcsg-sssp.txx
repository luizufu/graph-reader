#include <graph-reader/algo/vertex_centric_engine.hxx>
#include <cppcoro/sync_wait.hpp>
#include <numeric>

template <typename Graph>
vcsg_sssp<Graph>::vcsg_sssp(const Graph& graph, uint32_t s)
{
    vertex_centric_engine engine(graph);

    dist_prev default_value;
    default_value.dist = std::numeric_limits<float>::max();
    default_value.prev = 0;

    _info = cppcoro::sync_wait(engine.template run_scatter_gather<dist_prev, dist_prev>(
        // scatter
        [s](auto&& control) {

            // initialization step
            if(control.superstep() == 0 && control.id() == s)
                control.value() = { 0.f, control.id() };

            if(control.value().dist < std::numeric_limits<float>::max())
            {
                dist_prev msg;
                msg.prev = control.id();

                for(auto n : control.out_neighbours())
                {
                    msg.dist = control.value().dist + n.weight();
                    control.send_to(n.v, msg);
                }
            }
        },
        // gather
        [](auto&& control) {

            auto& msgs = control.messages();
            auto it = std::min_element(
                msgs.begin(), msgs.end(),
                [] (auto& lhs, auto& rhs) {
                    return lhs.dist < rhs.dist;
                });

            if(it != msgs.end() && it->dist < control.value().dist)
                control.value() = *it;
            else
                control.vote_to_halt();
        },
        // default state
        default_value));
}

template <typename Graph>
cppcoro::generator<std::vector<typename vcsg_sssp<Graph>::path_item_t>> vcsg_sssp<Graph>::traverse()
{
    size_t n_vertices = _info.size();

    for(uint32_t i = 0; i < n_vertices; ++i)
    {
        if(has_path_to(i))
        {
            std::vector<path_item_t> items;
            for(auto item : path_to(i))
                items.push_back(item);

            co_yield items;
        }
    }
}


template <typename Graph>
bool vcsg_sssp<Graph>::has_path_to(uint32_t v) const
{
    return _info[v].dist < std::numeric_limits<float>::max()
        && _info[v].prev != v;
}

template <typename Graph>
float vcsg_sssp<Graph>::dist_to(uint32_t v) const
{
    return _info[v].dist;
}

template <typename Graph>
cppcoro::generator<typename vcsg_sssp<Graph>::path_item_t> vcsg_sssp<Graph>::path_to(uint32_t v) const
{
    co_yield { v, _info[v].dist };

    while(_info[v].prev != v)
    {
        v = _info[v].prev;
        co_yield { v , _info[v].dist };
    }
}
