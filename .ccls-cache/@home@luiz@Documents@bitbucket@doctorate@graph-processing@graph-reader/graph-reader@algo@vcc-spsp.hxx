#pragma once

#include <cppcoro/generator.hpp>
#include <vector>

template <typename Graph>
class vcc_spsp
{
    struct dist_prev
    {
        float dist;
        uint32_t prev;
    };

    std::vector<dist_prev> _info;
    uint32_t _t;

    using path_item_t = std::pair<uint32_t, float>;

public:

    vcc_spsp(const Graph& graph, uint32_t s, uint32_t);

    cppcoro::generator<path_item_t> traverse();

    bool has_path_to(uint32_t v) const;
    float dist_to(uint32_t v) const;
    cppcoro::generator<path_item_t> path_to(uint32_t v) const;
};

