#include <graph-reader/aux/float-comparison.hxx>
#include <limits>
#include <numeric>
#include <queue>
#include <stack>

template <typename Graph>
seq_betweenness_centrality<Graph>::seq_betweenness_centrality(const Graph& graph)
    : _ranks(graph.n_vertices(), 0)
{
    size_t n_vertices = graph.n_vertices();

    // for min heap
    using path_item_t = std::pair<uint32_t, float>;
    auto comparator = [](const path_item_t& lhs, const path_item_t& rhs) {
        return lhs.second > rhs.second;
    };

    for(size_t s = 0; s < n_vertices; ++s)
    {
        std::vector<float> dist(n_vertices, std::numeric_limits<float>::max());
        std::vector<float> sigma(n_vertices, 0.f);
        std::vector<std::vector<uint32_t>> pred(n_vertices);
        std::stack<uint32_t> stack;

        std::priority_queue<
            path_item_t,
            std::vector<path_item_t>,
            decltype(comparator)> queue(comparator);

        dist[s] = 0.f;
        sigma[s] = 1.f;
        queue.push({ s, 0.f });

        while(!queue.empty())
        {
            uint32_t u = queue.top().first; queue.pop();
            stack.push(u);

            for(auto n : graph.out_neighbours(u))
            {
                if(dist[u] + n.weight() < dist[n.v])
                {
                    dist[n.v] = dist[u] + n.weight();
                    sigma[n.v] = 0.f;
                    pred[n.v].clear();
                    queue.push({ n.v, dist[n.v] });
                }

                if(approximately_equal(dist[n.v], dist[u] + n.weight()))
                {
                    sigma[n.v] += n.weight() * sigma[u];
                    pred[n.v].push_back(u);
                }
            }
        }

        std::vector<float> delta(n_vertices, 0);
        while(!stack.empty())
        {
            uint32_t v = stack.top(); stack.pop();

            for(uint32_t u : pred[v])
                delta[u] += (sigma[u] / sigma[v]) * (1 + delta[v]);

            if(v != s)
                _ranks[v] += delta[v];
        }
    }
}

template <typename Graph>
cppcoro::generator<typename seq_betweenness_centrality<Graph>::item> seq_betweenness_centrality<Graph>::traverse()
{
    for(size_t i = 0; i < _ranks.size(); ++i)
        co_yield { i, _ranks[i] };
}

