#include <cppcoro/sync_wait.hpp>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <mutex>

template <typename Graph>
vcc_bfs<Graph>::vcc_bfs(const Graph& graph, uint32_t s)
    : _engine(graph)
    , _s(s)
    , _visited(graph.n_vertices())
{
}


template <typename Graph>
cppcoro::generator<uint32_t> vcc_bfs<Graph>::traverse()
{
    auto order = cppcoro::sync_wait(_engine.template run_compute<uint32_t, uint32_t>(
        // compute
        [s = _s](auto&& control) {

            // if not visited yet
            if(control.value() == 0)
            {
                auto& msgs = control.messages();

                // if it's the source vertex at first iteration or
                // it's some vertex that received a new message
                if((control.superstep() == 0 && control.id() == s)
                    || !msgs.empty())
                {
                    // set my order to 1 + max element in messages
                    uint32_t my_order = 1;
                    if(!msgs.empty())
                        my_order += *std::max_element(msgs.begin(), msgs.end());

                    control.set_value(my_order);

                    // send my order to my neighbours
                    for(auto n : control.out_neighbours())
                        control.send_to(n.v, my_order);
                }
            }

            // i'm done
            control.vote_to_halt();
        },
        //default_value
        0));

    std::vector<uint32_t> inds(order.size());
    std::iota(inds.begin(), inds.end(), 0);

    std::sort(
        inds.begin(), inds.end(),
        [&order] (uint32_t lhs, uint32_t rhs) {
            return order[lhs] < order[rhs];
        });

    for(auto i : inds)
    {
        if(order[i] > 0)
        {
            _visited[i] = true;
            ++_count;
            co_yield i;
        }
    }
}

template <typename Graph>
size_t vcc_bfs<Graph>::count() const
{
    return _count;
}

template <typename Graph>
bool vcc_bfs<Graph>::visited(uint32_t v) const
{
    return _visited[v];
}
