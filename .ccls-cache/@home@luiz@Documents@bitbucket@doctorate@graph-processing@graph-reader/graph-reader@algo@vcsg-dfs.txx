#include <cppcoro/sync_wait.hpp>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <mutex>

template <typename Graph>
vcsg_dfs<Graph>::vcsg_dfs(const Graph& graph, uint32_t s)
    : _engine(graph)
    , _graph(graph)
    , _s(s)
{
}


template <typename Graph>
cppcoro::generator<uint32_t> vcsg_dfs<Graph>::traverse()
{
    auto order = cppcoro::sync_wait(_engine.template run_scatter_gather<uint32_t, uint32_t>(
        // scatter
        [s = _s](auto&& control) {

            if(control.id() == s && control.superstep() == 0)
                control.value() = 1;

            if(control.value() > 0)
            {
                size_t i = 0;
                uint32_t highest =
                    control.aggregated() -
                    control.out_degree();

                for(auto n : control.out_neighbours())
                    control.send_to(n.v, control.value() + 2 + highest * i++);
            }

        },
        // gather
        [](auto&& control) {

            auto& msgs = control.messages();

            if(control.value() == 0 && !msgs.empty())
                control.value() = *std::max_element(msgs.begin(), msgs.end());
            else
                control.vote_to_halt();
        },
        // default state
        0,
        // aggregator
        [](auto& states) {
            return std::count_if(
                states.begin(), states.end(),
                [](uint32_t state) {
                    return state == 0;
                });
        },
        // default aggregated
        _graph.n_vertices()));

    std::vector<uint32_t> inds(order.size());
    std::iota(inds.begin(), inds.end(), 0);

    std::sort(
        inds.begin(), inds.end(),
        [&order] (uint32_t lhs, uint32_t rhs) {
            return order[lhs] < order[rhs];
        });

    for(auto i : inds)
    {
        if(order[i] > 0)
        {
            ++_count;
            co_yield i;
        }
    }
}

template <typename Graph>
size_t vcsg_dfs<Graph>::count() const
{
    return _count;
}

template <typename Graph>
bool vcsg_dfs<Graph>::visited(uint32_t v) const
{
    return _visited[v];
}
