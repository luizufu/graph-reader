#pragma once

#define INSTATIATE_GRAPH_TEMPLATES(graph) \
    template class graph<edge>; \
    template class graph<weighted_edge>;

#define INSTATIATE_GRAPH_TEMPLATES_EXTERN(graph) \
    extern template class stg::graph<stg::edge>; \
    extern template class stg::graph<stg::weighted_edge>;
