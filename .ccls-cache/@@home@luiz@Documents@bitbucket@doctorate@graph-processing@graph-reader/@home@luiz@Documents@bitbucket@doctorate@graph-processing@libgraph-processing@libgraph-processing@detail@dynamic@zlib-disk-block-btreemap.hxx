#pragma once

#include <libgraph-processing/config.hxx>
#include <libgraph-processing/detail/macro/dynamic1c.hxx>
#include <vector>
#include <optional>
#include <cppcoro/generator.hpp>

namespace stg::detail
{

template <typename Key, typename Value, typename BlockAllocator>
class zlib_disk_block_btreemap
{

    enum block_type : uint32_t
    {
        internal,
        external
    };

    struct ublock
    {
        uint32_t type;
        std::vector<Key> keys;
        std::vector<Value> values;
        std::vector<uint32_t> pointers;
    };

    struct alignas(PAGE_SIZE) block
    {
        uint32_t size;
        std::byte data[PAGE_SIZE - sizeof(uint32_t)];
    };

    struct update_result
    {
        bool updated_parent;
        bool succeeded;
    };


    static void compress(ublock& ub, block& b);
    static void uncompress(block& b, ublock& ub);

    static size_t find_eq(const Key& key, const ublock& ub);
    static size_t find_geq(const Key& key, const ublock& ub);

    static update_result insert_rec(BlockAllocator& alloc,
            uint32_t index, ublock& parent,
            const Key& key, const Value& value);

    static update_result remove_rec(BlockAllocator& alloc,
            uint32_t index, ublock& parent,
            const Key& key, Value& value,
            uint32_t& root);

public:

    struct location
    {
        uint32_t block;
        uint32_t pos;
    };

    static uint32_t create(BlockAllocator& alloc);
    static void destroy(BlockAllocator& alloc, uint32_t root);

    static bool insert(BlockAllocator& alloc, uint32_t& root,
                       const Key& key, const Value& value);

    template <typename Updater>
    static bool update_value(BlockAllocator& alloc, uint32_t& root,
                             Updater updater);

    template <typename Updater>
    static bool update_value(BlockAllocator& alloc, uint32_t& root,
                             const Key& key, Updater updater);

    template <typename Updater>
    static bool update_value_direct(BlockAllocator& alloc, const location& loc,
                                    Updater updater);

    static std::optional<Value> remove(BlockAllocator& alloc,uint32_t& root,
                                       const Key& key);

    static std::vector<std::pair<Key, Value>>
        remove(BlockAllocator& alloc, uint32_t& root,
               const std::vector<Key>& keys);

    static std::optional<Value> find(BlockAllocator& alloc, uint32_t root,
                                     const Key& key);

    static std::optional<location>
        find_location(BlockAllocator& alloc, uint32_t root, const Key& key);

    static cppcoro::generator<std::pair<Key, Value>>
        all(BlockAllocator& alloc, uint32_t first_leaf);

    static bool exists(BlockAllocator& alloc, uint32_t root,
                       const Key& key);

    static size_t size(BlockAllocator& alloc, uint32_t first_leaf);
    static bool empty(BlockAllocator& alloc, uint32_t first_leaf);

    static void debug(BlockAllocator& alloc,
                      uint32_t root, uint32_t first_leaf);
};


} /* stg */

#include <libgraph-processing/detail/dynamic/zlib-disk-block-btreemap.ixx>

INSTATIATE_LV1_COMPRESSED_STRUCT_TEMPLATES_EXTERN(zlib_disk_block_btreemap)
