#pragma once

#include <libgraph-processing/config.hxx>
#include <libgraph-processing/detail/macro/dynamic2.hxx>
#include <cppcoro/generator.hpp>
#include <vector>
#include <iostream>

namespace stg::detail
{


template <typename Key, typename BlockAllocator>
class disk_block_btreeset
{
    static constexpr uint32_t N_IKEYS =
        (PAGE_SIZE - 2*sizeof(uint32_t)) /
        (sizeof(Key) + sizeof(uint32_t)) - 1;

    static constexpr uint32_t N_EKEYS =
        (PAGE_SIZE - 3*sizeof(uint32_t)) / (sizeof(Key)) - 1;

    struct iblock
    {
        Key keys[N_IKEYS + 1];
        uint32_t pointers[N_IKEYS + 1];
        uint32_t size;
    };

    struct eblock
    {
        Key keys[N_EKEYS + 1];
        uint32_t pointer;
        uint32_t size;
    };

    struct alignas(PAGE_SIZE) block
    {
        union
        {
            iblock i;
            eblock e;
        };

        uint32_t type;
    };


public:



    static uint32_t create(BlockAllocator& alloc);
    static void destroy(BlockAllocator& alloc, uint32_t root);

    static bool insert(BlockAllocator& alloc, uint32_t& root, const Key& key);

    static bool remove(BlockAllocator& alloc,uint32_t& root,
                       const Key& key);

    static std::vector<Key> remove(BlockAllocator& alloc,uint32_t& root,
                                   const std::vector<Key>& keys);

    static std::vector<Key> remove_all(BlockAllocator& alloc,
                                       uint32_t& root, uint32_t first_leaf);

    static cppcoro::generator<Key> all(BlockAllocator& alloc,
                                       uint32_t first_leaf);

    static bool exists(BlockAllocator& alloc, uint32_t root, const Key& key);

    static size_t size(BlockAllocator& alloc, uint32_t first_leaf);
    static bool empty(BlockAllocator& alloc, uint32_t first_leaf);

    static void debug(BlockAllocator& alloc, uint32_t root,uint32_t first_leaf);
};


} /* stg */

INSTATIATE_LV2_STRUCT_TEMPLATES_EXTERN(disk_block_btreeset)
