#pragma once

#include <libgraph-processing/edge.hxx>
#include <libgraph-processing/headers.hxx>
#include <libgraph-processing/detail/dynamic/disk-block-allocator.hxx>

#define INSTATIATE_LV2_STRUCT_TEMPLATES(struct) \
    template class struct<neighbor, \
        disk_block_allocator<adj_list_header>>; \
    template class struct<neighbor, \
        disk_block_allocator<edge_grid_header>>; \
    template class struct<weighted_neighbor, \
        disk_block_allocator<adj_list_header>>; \
    template class struct<weighted_neighbor, \
        disk_block_allocator<edge_grid_header>>; \
    template class struct<edge, \
        disk_block_allocator<adj_list_header>>; \
    template class struct<edge, \
        disk_block_allocator<edge_grid_header>>; \
    template class struct<weighted_edge, \
        disk_block_allocator<adj_list_header>>; \
    template class struct<weighted_edge, \
        disk_block_allocator<edge_grid_header>>;


#define INSTATIATE_LV2_STRUCT_TEMPLATES_EXTERN(struct) \
    extern template class stg::detail::struct<stg::neighbor, \
        stg::detail::disk_block_allocator<stg::adj_list_header>>; \
    extern template class stg::detail::struct<stg::neighbor, \
        stg::detail::disk_block_allocator<stg::edge_grid_header>>; \
    extern template class stg::detail::struct<stg::weighted_neighbor, \
        stg::detail::disk_block_allocator<stg::adj_list_header>>; \
    extern template class stg::detail::struct<stg::weighted_neighbor, \
        stg::detail::disk_block_allocator<stg::edge_grid_header>>; \
    extern template class stg::detail::struct<stg::edge, \
        stg::detail::disk_block_allocator<stg::adj_list_header>>; \
    extern template class stg::detail::struct<stg::edge, \
        stg::detail::disk_block_allocator<stg::edge_grid_header>>; \
    extern template class stg::detail::struct<stg::weighted_edge, \
        stg::detail::disk_block_allocator<stg::adj_list_header>>; \
    extern template class stg::detail::struct<stg::weighted_edge, \
        stg::detail::disk_block_allocator<stg::edge_grid_header>>;
