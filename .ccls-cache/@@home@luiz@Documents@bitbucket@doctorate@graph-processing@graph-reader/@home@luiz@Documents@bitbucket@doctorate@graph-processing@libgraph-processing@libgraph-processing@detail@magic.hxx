#pragma once

#include <cstdint>

namespace stg
{

const uint32_t EDGE_LIST_MAGIC = 0x47724166;
const uint32_t ADJ_LIST_MAGIC = 0x67526146;
const uint32_t EDGE_GRID_MAGIC = 0x47526166;

} /* stg */
