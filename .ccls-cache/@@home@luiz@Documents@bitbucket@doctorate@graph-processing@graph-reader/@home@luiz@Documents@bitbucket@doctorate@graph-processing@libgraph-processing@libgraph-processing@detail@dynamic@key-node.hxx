#pragma once

#include <cstdint>
#include <ostream>

namespace stg::detail
{

struct key_node
{
    uint32_t total;
    uint32_t root;
    uint32_t leaf;

} __attribute__ ((packed));


struct unordered_key_node
{
    uint32_t total;
    uint32_t block;

} __attribute__ ((packed));


struct zlib_key_node
{
    uint32_t total;
    uint32_t root;
    uint32_t leaf;

} __attribute__ ((packed));


struct zlib_unordered_key_node
{
    uint32_t total;
    uint32_t block;

} __attribute__ ((packed));


std::ostream& operator<<(std::ostream& out, const key_node& kn);
std::ostream& operator<<(std::ostream& out, const unordered_key_node& kn);
std::ostream& operator<<(std::ostream& out, const zlib_key_node& kn);
std::ostream& operator<<(std::ostream& out, const zlib_unordered_key_node& kn);


} /* stg */
