#include <libgraph-processing/detail/util.hxx>

namespace stg::detail
{

template <typename Key, typename Value, typename BlockAllocator>
template <typename Predicate>
std::vector<Value> zlib_disk_block_multimap<Key, Value, BlockAllocator>::remove_if(BlockAllocator& alloc, uint32_t root, Predicate pred)
{
    std::vector<Value> removed;

    if(!root)
        return removed;

    block b;
    alloc.get().read(root, to_bytes(b));
    uint32_t old_root = b.kmap_root;

    zlib_key_node kn = { .total = 1 };
    Key keyo = {};
    kmap_t::update_value(alloc, b.kmap_root,
        [&] (const Key& key, auto& node) {
            bool updated = false;
            for(auto v : vset_t::all(alloc, node.leaf))
            {
                if(pred(v) && vset_t::remove(alloc, node.root, v))
                {
                    removed.push_back(v);
                    --node.total;
                    --b.total;
                    updated = true;
                }
            }

            kn = node;
            keyo = key;
            return updated;
        });

    if(kn.total == 0)
    {
        vset_t::destroy(alloc, kn.root);
        kmap_t::remove(alloc, b.kmap_root, keyo);
    }

    if(b.kmap_root != old_root || !removed.empty())
        alloc.get().write(root, to_bytes(b));

    return removed;
}

template <typename Key, typename Value, typename BlockAllocator>
template <typename Predicate>
std::vector<Value> zlib_disk_block_multimap<Key, Value, BlockAllocator>::remove_if(BlockAllocator& alloc, uint32_t root, const Key& key, Predicate pred)
{
    std::vector<Value> removed;

    if(!root)
        return removed;

    block b;
    alloc.get().read(root, to_bytes(b));
    uint32_t old_root = b.kmap_root;

    zlib_key_node kn = { .total = 1 };
    kmap_t::update_value(alloc, b.kmap_root, key,
        [&] (auto& node) {
            bool updated = false;
            for(auto v : vset_t::all(alloc, node.leaf))
            {
                if(pred(v) && vset_t::remove(alloc, node.root, v))
                {
                    removed.push_back(v);
                    --node.total;
                    --b.total;
                    updated = true;
                }
            }

            kn = node;
            return updated;
        });

    if(kn.total == 0)
    {
        vset_t::destroy(alloc, kn.root);
        kmap_t::remove(alloc, b.kmap_root, key);
    }

    if(b.kmap_root != old_root || !removed.empty())
        alloc.get().write(root, to_bytes(b));

    return removed;
}

} /* stg */
