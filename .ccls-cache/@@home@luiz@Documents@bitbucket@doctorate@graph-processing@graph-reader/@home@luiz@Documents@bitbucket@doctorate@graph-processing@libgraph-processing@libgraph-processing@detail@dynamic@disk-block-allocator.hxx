#pragma once

#include <libgraph-processing/config.hxx>
#include <libgraph-processing/headers.hxx>
#include <cstddef>
#include <cstdint>

namespace stg::detail
{

template <typename Header>
class disk_block_allocator
{
    class disk_block_vector;

    struct alignas(PAGE_SIZE) super_block
    {
        Header header;
        uint32_t n_blocks;
        uint32_t key_list1;
        uint32_t key_list2;
        uint32_t key_list3;
        uint32_t key_list4;
        uint32_t key_list5;
        uint32_t free_list;
    };

    struct alignas(PAGE_SIZE) deleted_block
    {
        uint32_t next;
    };

    super_block _super_block;
    disk_block_vector _blocks;


public:

    disk_block_allocator(const char* filename);
    ~disk_block_allocator();

    uint32_t new_block();
    // let only this version, use an deleted block alrdy allocated to write at i
    void delete_block(uint32_t i);
    void delete_block(uint32_t i, const std::byte* block);

    struct super_block& super_block();
    const struct super_block& super_block() const;

    disk_block_vector& get();
};

template <typename Header>
class disk_block_allocator<Header>::disk_block_vector
{
    int _fd;

    mutable std::byte _cache[PAGE_SIZE];
    mutable size_t _current_block;
    size_t _size;

    /* class const_iterator */
    /* { */
    /*     const disk_block_vector& _ref; */
    /*     size_t _i; */

    /*     public: */
    /*         using self_type = const_iterator; */
    /*         using value_type = Block; */
    /*         using reference = const Block&; */
    /*         using pointer = const Block*; */
    /*         using iterator_category = std::input_iterator_tag; */
    /*         using difference_type = int; */

    /*         const_iterator(const disk_block_vector& ref, size_t i) : _ref(ref), _i(i) { } */
    /*         self_type operator++() { self_type it = *this; ++_i; return it; } */
    /*         self_type operator++(int junk) { ++_i; return *this; } */
    /*         reference operator*() { return _ref[_i]; } */
    /*         pointer operator->() { return &_ref[_i]; } */
    /*         bool operator==(const self_type& rhs) { return _ref._fd == rhs._ref._fd && _i == rhs._i; } */
    /*         bool operator!=(const self_type& rhs) { return !operator==(rhs); } */
    /* }; */

public:

    disk_block_vector(const char* filename);
    ~disk_block_vector();

    void read(size_t i, std::byte* block) const;

    void write(size_t i, const std::byte* block);
    void push_back(const std::byte* block);
    void pop_back();

    void resize(size_t n);
    void clear();

    size_t size() const;
    bool empty() const;

    /* const_iterator begin() const; */
    /* const_iterator end() const; */
};


} /* stg */

extern template class stg::detail::disk_block_allocator<stg::adj_list_header>;
extern template class stg::detail::disk_block_allocator<stg::edge_grid_header>;

