#include <libgraph-processing/detail/util.hxx>

namespace stg::detail
{

template <typename Key, typename Value, typename BlockAllocator>
template <typename Updater>
bool zlib_disk_block_btreemap<Key, Value, BlockAllocator>::update_value(BlockAllocator& alloc,uint32_t& root, Updater updater)
{
    if(!root)
        return false;

    block b;
    alloc.get().read(root, to_bytes(b));
    ublock ub;
    uncompress(b, ub);

    if(ub.type == block_type::external)
    {
        if(ub.keys.empty())
            return false;

        bool updated = false;
        for(size_t i = 0; i < ub.keys.size(); ++i)
            updated |= updater(ub.keys[i], ub.values[i]);

        if(!updated)
            return false;

        compress(ub, b);
        alloc.get().write(root, to_bytes(b));
        return true;
    }

    bool updated = false;
    for(size_t i = 0; i < ub.pointers.size(); ++i)
        updated |= update_value(alloc, ub.pointers[i], updater);

    return updated;
}

template <typename Key, typename Value, typename BlockAllocator>
template <typename Updater>
bool zlib_disk_block_btreemap<Key, Value, BlockAllocator>::update_value(BlockAllocator& alloc,uint32_t& root, const Key& key, Updater updater)
{
    if(!root)
        return false;

    block b;
    alloc.get().read(root, to_bytes(b));
    ublock ub;
    uncompress(b, ub);

    if(ub.type == block_type::external)
    {
        if(ub.keys.empty())
            return false;

        size_t i = find_eq(key, ub);
        if(i == ub.keys.size())
            return false;

        if(!updater(ub.values[i]))
            return false;

        compress(ub, b);
        alloc.get().write(root, to_bytes(b));
        return true;
    }

    size_t i = find_geq(key, ub);
    return update_value(alloc, ub.pointers[i], key, updater);
}

template <typename Key, typename Value, typename BlockAllocator>
template <typename Updater>
bool zlib_disk_block_btreemap<Key, Value, BlockAllocator>::update_value_direct(BlockAllocator& alloc, const location& loc, Updater updater)
{
    if(!loc.block)
        return false;

    block b;
    alloc.get().read(loc.block, to_bytes(b));
    ublock ub;
    uncompress(b, ub);

    if(ub.type != block_type::external)
        return false;

    if(loc.pos >= ub.keys.size())
        return false;

    if(!updater(ub.values[loc.pos]))
        return false;

    compress(ub, b);
    alloc.get().write(loc.block, to_bytes(b));
    return true;
}

} /* stg */
