#pragma once

#include <libgraph-processing/config.hxx>
#include <libgraph-processing/detail/dynamic/zlib-disk-block-btreemap.hxx>
#include <libgraph-processing/detail/dynamic/zlib-disk-block-btreeset.hxx>
#include <libgraph-processing/detail/dynamic/key-node.hxx>
#include <libgraph-processing/detail/macro/dynamic0.hxx>
#include <vector>
#include <cppcoro/generator.hpp>

namespace stg::detail
{

std::ostream& operator<<(std::ostream& out, const zlib_key_node& kn);

template <typename Key, typename Value, typename BlockAllocator>
class zlib_disk_block_multimap
{
    struct alignas(PAGE_SIZE) block
    {
        uint32_t kmap_root;
        uint32_t kmap_leaf;
        uint32_t total;
    };

    using kmap_t = zlib_disk_block_btreemap<Key, zlib_key_node,
          BlockAllocator>;
    using vset_t = zlib_disk_block_btreeset<Value, BlockAllocator>;

public:


    static size_t create(BlockAllocator& alloc);
    static void destroy(BlockAllocator& alloc, uint32_t root);

    static bool insert(BlockAllocator& alloc, uint32_t root,
            const Key& key, const Value& value);
    static size_t insert(BlockAllocator& alloc, uint32_t root,
            const Key& key, std::vector<Value>& values);

    static std::vector<Value> remove(BlockAllocator& alloc, uint32_t root,
            const Key& key);
    static bool remove(BlockAllocator& alloc, uint32_t root,
            const Key& key, const Value& value);
    static std::vector<Value> remove(BlockAllocator& alloc, uint32_t root,
            const Key& key, std::vector<Value>& values);

    template <typename Predicate>
    static std::vector<Value> remove_if(BlockAllocator& alloc, uint32_t root,
            Predicate pred);

    template <typename Predicate>
    static std::vector<Value> remove_if(BlockAllocator& alloc, uint32_t root,
            const Key& key, Predicate pred);

    static cppcoro::generator<Value> find(BlockAllocator& alloc, uint32_t root,
            const Key& key);
    static cppcoro::generator<std::pair<Key, Value>> all(BlockAllocator& alloc, uint32_t root);
    static cppcoro::generator<Key> all_keys(BlockAllocator& alloc, uint32_t root);
    static size_t count(BlockAllocator& alloc, uint32_t root, const Key& key);

    static bool exists(BlockAllocator& alloc, uint32_t root,
            const Key& key, const Value& value);

    static size_t size(BlockAllocator& alloc, uint32_t root);
    static bool empty(BlockAllocator& alloc, uint32_t root);

    static void debug(BlockAllocator& alloc, uint32_t root);
};


} /* stg */

#include <libgraph-processing/detail/dynamic/zlib-disk-block-multimap.ixx>

INSTATIATE_LV0_STRUCT_TEMPLATES_EXTERN(zlib_disk_block_multimap)
