#pragma once

#include <libgraph-processing/config.hxx>
#include <libgraph-processing/detail/macro/dynamic2.hxx>
#include <vector>
#include <cppcoro/generator.hpp>

namespace stg::detail
{

template <typename Key, typename BlockAllocator>
class zlib_disk_block_btreeset
{
    struct ublock
    {
        uint32_t type;
        std::vector<Key> keys;
        std::vector<uint32_t> pointers;
    };

    struct alignas(PAGE_SIZE) block
    {
        uint32_t size;
        std::byte data[PAGE_SIZE - sizeof(uint32_t)];
    };

public:


    static uint32_t create(BlockAllocator& alloc);
    static void destroy(BlockAllocator& alloc, uint32_t root);

    static bool insert(BlockAllocator& alloc, uint32_t& root, const Key& key);

    static bool remove(BlockAllocator& alloc,uint32_t& root,
                       const Key& key);

    static std::vector<Key> remove(BlockAllocator& alloc,uint32_t& root,
                                   const std::vector<Key>& keys);

    static std::vector<Key> remove_all(BlockAllocator& alloc,
                                       uint32_t& root, uint32_t first_leaf);

    static cppcoro::generator<Key> all(BlockAllocator& alloc,
                                       uint32_t first_leaf);

    static bool exists(BlockAllocator& alloc, uint32_t root, const Key& key);

    static size_t size(BlockAllocator& alloc, uint32_t first_leaf);
    static bool empty(BlockAllocator& alloc, uint32_t first_leaf);

    static void debug(BlockAllocator& alloc, uint32_t root,uint32_t first_leaf);
};


} /* stg */

INSTATIATE_LV2_STRUCT_TEMPLATES_EXTERN(zlib_disk_block_btreeset)
