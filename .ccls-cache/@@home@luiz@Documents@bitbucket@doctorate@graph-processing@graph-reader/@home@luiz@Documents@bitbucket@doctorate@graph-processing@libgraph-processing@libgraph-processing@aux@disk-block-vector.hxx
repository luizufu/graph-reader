#pragma once

#include <stdexcept>
#include <iterator>
#include <cstddef>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>

template <typename Block>
class disk_block_vector
{
    int _fd;

    mutable Block _cache;
    mutable size_t _current_block;
    size_t _size;

    class const_iterator
    {
        const disk_block_vector& _ref;
        size_t _i;

        public:
            using self_type = const_iterator;
            using value_type = Block;
            using reference = const Block&;
            using pointer = const Block*;
            using iterator_category = std::input_iterator_tag;
            using difference_type = int;

            const_iterator(const disk_block_vector& ref, size_t i) : _ref(ref), _i(i) { }
            self_type operator++() { self_type it = *this; ++_i; return it; }
            self_type operator++(int junk) { ++_i; return *this; }
            reference operator*() { return _ref[_i]; }
            pointer operator->() { return &_ref[_i]; }
            bool operator==(const self_type& rhs) { return _ref._fd == rhs._fd && _i == rhs._i; }
            bool operator!=(const self_type& rhs) { return !operator==(); }
    };

public:

    disk_block_vector(const char* filename)
    {
        _fd = open(filename, O_CREAT | O_RDWR, 0666);
        _size = lseek(_fd, 0, SEEK_END) / sizeof(Block);
    }

    ~disk_block_vector()
    {
        close(_fd);
    }


    const Block& at(size_t i) const
    {
        if(i >= _size)
            return std::out_of_range("No block at this index");

        if(i == _current_block)
            return _cache;

        lseek(_fd, i * sizeof(Block), SEEK_SET);
        read(_fd, reinterpret_cast<char*>(&_cache), sizeof(_cache));

        _current_block = i;

        return _cache;
    }

    const Block& operator[](size_t i) const
    {
        if(i == _current_block)
            return _cache;

        lseek(_fd, i * sizeof(Block), SEEK_SET);
        read(_fd, reinterpret_cast<char*>(&_cache), sizeof(_cache));

        _current_block = i;

        return _cache;
    }

    const Block& front() const
    {
        if(_size == 0)
            return std::out_of_range("File is empty");

        return operator[](0);
    }

    const Block& back() const
    {
        if(_size == 0)
            return std::out_of_range("File is empty");

        return operator[](_size - 1);
    }

    void write(size_t i, const Block& block)
    {
        lseek(_fd, i * sizeof(Block), SEEK_SET);
        ::write(_fd, reinterpret_cast<const char*>(&block), sizeof(block));

        _cache = block;
        _current_block = i;
    }

    void push_back(const Block& block)
    {
        _cache = block;

        lseek(_fd, _size * sizeof(Block), SEEK_SET);
        ::write(_fd, reinterpret_cast<const char*>(&block), sizeof(block));

        _cache = block;
        _current_block = _size;

        ++_size;
    }

    void pop_back()
    {
        resize(_size - 1);
    }

    void resize(size_t n)
    {
        ftruncate(_fd, ((off_t) n) * sizeof(Block));

        _size = n;
    }

    void clear()
    {
        resize(0);
    }

    size_t size() const
    {
        return _size;
    }

    bool empty() const
    {
        return _size == 0;
    }

    const_iterator begin() const
    {
        return const_iterator(*this, 0);
    }

    const_iterator end() const
    {
        return const_iterator(*this, _size);
    }
};
