#pragma once

#include <libgraph-processing/config.hxx>
#include <libgraph-processing/detail/macro/dynamic2.hxx>
#include <vector>
#include <cppcoro/generator.hpp>

namespace stg::detail
{

template <typename T, typename BlockAllocator>
class disk_block_list
{
    static const size_t N_ELEMENTS =
        (PAGE_SIZE - 2*sizeof(uint32_t)) / sizeof(T);

    struct alignas(PAGE_SIZE) block
    {
        T elements[N_ELEMENTS];
        uint32_t size;
        uint32_t next;
    };

public:

    static uint32_t create(BlockAllocator& alloc);
    static void destroy(BlockAllocator& alloc, uint32_t root);

    static std::pair<uint32_t, size_t>
        push(BlockAllocator& alloc, uint32_t root,
            const T& element);
    static void pop(BlockAllocator& alloc, uint32_t& root);

    // change order of last element, so pop will not remove the last
    // pushed value
    static bool remove(BlockAllocator& alloc, uint32_t& root,
            const T& element);

    // change order of last element, so pop will not remove the last
    // pushed value
    static std::vector<T> remove(BlockAllocator& alloc, uint32_t& root,
            std::vector<T>& elements);

    // change order of last element, so pop will not remove the last
    // pushed value
    template <typename Predicate>
    static std::vector<T> remove_if(BlockAllocator& alloc, uint32_t& root,
            Predicate pred);


    static std::vector<T> remove_all(BlockAllocator& alloc, uint32_t root);

    template <typename UpdateFunction>
    static void try_update(BlockAllocator& alloc, uint32_t root,
            const T& element, UpdateFunction update);

    template <typename UpdateFunction>
    static void direct_update(BlockAllocator& alloc, uint32_t block,
            UpdateFunction update);

    template <typename UpdateFunction>
    static void direct_update(BlockAllocator& alloc, uint32_t block, size_t pos,
            UpdateFunction update);

    static T direct_access(BlockAllocator& alloc, uint32_t block, size_t pos);


    static cppcoro::generator<T> all(BlockAllocator& alloc, uint32_t root);

    static bool exists(BlockAllocator& alloc, uint32_t root, const T& element);

    static size_t size(BlockAllocator& alloc, uint32_t root);
    static bool empty(BlockAllocator& alloc, uint32_t root);

    static void debug(BlockAllocator& alloc, uint32_t root);
};

} /* stg */

#include <libgraph-processing/detail/dynamic/disk-block-list.ixx>

INSTATIATE_LV2_STRUCT_TEMPLATES_EXTERN(disk_block_list)
